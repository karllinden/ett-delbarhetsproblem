#!/usr/bin/env python
# encoding: utf-8
#
#  Copyright 2015 Karl Lindén <karl.j.linden@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

from waflib import Options, TaskGen
from waflib.Tools import waf_unit_test

VERSION='1.0.0'
APPNAME='ett-delbarhetsproblem'

# these variables are mandatory ('/' are converted automatically)
top = '.'
out = 'build'

auto_options = []
g_maxlen = 40
extra_cflags = [
    '-fipa-pure-const',
    '-fstrict-overflow'
]
warning_cflags = [
    '-Wall',
    '-Wconversion',
    '-Winline',
    '-Wmissing-declarations',
    '-Wshadow',
    '-Wsign-compare',
    '-Wsign-conversion',
    '-Wstrict-overflow=5',
    '-Wsuggest-attribute=const',
    '-Wsuggest-attribute=pure',
    '-Wtype-limits',
    '-pedantic'
]

candcount_gnuplot = '''
set terminal epslatex
set output "{}"
set size .9
set monochrome
set palette gray
set key top left
set logscale y
set xlabel "$n$"
set ylabel "$y$
plot \\
    "{}" title "$y = |\\\\mathbb{{K}}_n|$", \\
    2**x title "$y = 2^n$"
'''

def options(opt):
    opt.load('compiler_c')
    opt.load('tex')
    opt.load('waf_unit_test')

    opt.add_option('--debug', action='store_true', default=False,
                   help='Enable debug messages and run-time self-tests')
    opt.add_option('--no-optimize', action='store_true', default=False,
                   help='Disable compiler optimizations')

def check_sizeof(conf, ctype):
    conf.start_msg('Checking size of {}'.format(ctype))
    result = conf.check(
        fragment    = '''
                      #include <stdio.h>
                      int main(void) {{
                         printf("%ld", sizeof({}));
                         return 0;
                      }}
                      '''.format(ctype),
        execute     = True,
        define_name = 'SIZEOF_{}'.format(ctype.upper().replace(' ', '_')),
        quote       = False,
        define_ret  = True,
        quiet       = True)
    conf.end_msg(result, color='NORMAL')

def configure(conf):
    conf.load('compiler_c')
    conf.load('tex')
    conf.load('waf_unit_test')

    conf.env.append_unique('CFLAGS', '-std=gnu11')
    conf.env.append_unique('CFLAGS', extra_cflags)
    conf.env.append_unique('CFLAGS', warning_cflags)

    conf.env.append_unique('LATEXFLAGS', '-halt-on-error')

    check_sizeof(conf, 'unsigned long')
    for f in ['__builtin_clzl', '__builtin_ctzl', '__builtin_popcountl']:
        conf.check(
            fragment    = '''
                          int main(void) {{
                              {}(0);
                              return 0;
                          }}
                          '''.format(f),
            define_name = 'HAVE_' + f.upper(),
            execute     = False,
            msg         = 'Checking for ' + f)

    conf.define('PACKAGE', APPNAME)
    conf.define('VERSION', VERSION)

    conf.check_cfg(package='gop-3', uselib_store='GOP',
                   args='--cflags --libs')

    if Options.options.debug:
        Options.options.no_optimize = True
        conf.define('DEBUG', 1)
        conf.env.append_unique('CFLAGS', '-ggdb')
    else:
        conf.define('NDEBUG', 1)

    if not Options.options.no_optimize:
        conf.env.append_unique('CFLAGS', '-O3')

    conf.write_config_header('config.h')
    conf.env.append_unique('CFLAGS', '-DHAVE_CONFIG_H=1')

@TaskGen.feature('c')
@TaskGen.after_method('apply_incpaths')
def insert_blddir(self):
    self.env.prepend_value('INCPATHS', self.bld.bldnode.abspath())

def build_candcount(bld):
    name    = 'candcount'
    program = name + '-simple'
    datfile = name + '.dat'
    texfile = name + '.tex'

    candcount = bld.bldnode.find_or_declare(program)
    args = ' '.join([str(n) for n in range(1,45)])
    bld(
        rule   = candcount.abspath() + ' -o ${TGT} ' + args,
        deps   = [program],
        target = datfile
    )

    gpfile = bld.bldnode.make_node(name + '.gnuplot')
    gpfile.write(candcount_gnuplot.format(texfile, datfile))

    bld(
        rule   = 'gnuplot {}'.format(gpfile.abspath()),
        target = texfile,
        source = datfile,
        deps   = ['wscript']
    )

    return texfile

def build_phitab_tex(bld):
    name    = 'phitab'
    program = name
    texfile = name + '.tex'

    phitab = bld.bldnode.find_or_declare('phitab')
    bld(
        rule   = phitab.abspath() + ' -o ${TGT} -c 2 1 23 2',
        deps   = [program],
        target = texfile
    )

    return texfile

def build(bld):
    # FIXME: Probably split into more libs when implementing faster
    #        zpsets to avoid recompiling the invariant sources a lot.
    bld(
        features = 'c cstlib',
        target   = 'zpset',
        source   = [
                    'src/digits.c',
                    'src/dist.c',
                    'src/table.c',
                    'src/zpset.c',
                    'src/zpset-io.c',
                    'src/zpset-list.c'
                   ]
    )

    bld(
        features = 'c cstlib',
        target   = 'alg',
        source   = 'src/alg.c',
        use      = ['zpset']
    )

    bld(
        features = 'c cstlib',
        target   = 'phi',
        source   = 'src/phi.c',
        use      = ['zpset']
    )

    bld(
        features = 'c cprogram',
        target   = 'phi-print',
        source   = 'src/phi-print.c',
        use      = ['phi', 'zpset', 'GOP']
    )

    bld(
        features = 'c cprogram',
        target   = 'phitab',
        source   = 'src/phitab.c',
        use      = ['phi', 'zpset', 'GOP']
    )

    algs = [
        'opti1',
        'opti2',
        'simple',
    ]
    for a in algs:
        tgt = 'alg-' + a
        bld(
            features = 'c cstlib',
            target   = tgt,
            source   = 'src/{}.c'.format(tgt),
            use      = ['zpset']
        )
        bld(
            features = 'c cprogram',
            target   = 'candcount-' + a,
            source   = 'src/candcount.c',
            use      = [tgt, 'alg', 'zpset', 'GOP']
        )
        bld(
            features = 'c cprogram',
            target   = 'print-' + a,
            source   = 'src/print.c',
            use      = [tgt, 'alg', 'zpset', 'GOP']
        )
        bld(
            features = 'c cprogram',
            target   = 'time-' + a,
            source   = 'src/time.c',
            use      = [tgt, 'alg', 'zpset', 'GOP']
        )

    bld(
        features = 'c cstlib',
        target   = 'rng',
        source   = [
                    'tests/rng.c',
                    'tests/rng-dta.c'
                   ],
        includes = ['src'],
        use      = ['GOP']
    )

    bld(
        features = 'c cprogram test',
        target   = 'zpset-test',
        source   = 'tests/zpset-test.c',
        includes = ['src'],
        use      = ['rng', 'zpset', 'GOP']
    )
    bld(
        features = 'c cprogram test',
        target   = 'zpset-dta-min',
        source   = 'tests/zpset-dta-min.c',
        includes = ['src'],
        use      = ['rng', 'zpset', 'GOP']
    )
    bld(
        features = 'c cprogram test',
        target   = 'zpset-dta-max',
        source   = 'tests/zpset-dta-max.c',
        includes = ['src'],
        use      = ['rng', 'zpset', 'GOP']
    )
    bld(
        features = 'c cprogram test',
        target   = 'zpset-init-full',
        source   = 'tests/zpset-init-full.c',
        includes = ['src'],
        use      = ['rng', 'zpset', 'GOP']
    )
    bld(
        features = 'c cprogram test',
        target   = 'zpset-remove-less',
        source   = 'tests/zpset-remove-less.c',
        includes = ['src'],
        use      = ['zpset']
    )
    bld(
        features = 'c cprogram test',
        target   = 'zpset-remove-multi',
        source   = 'tests/zpset-remove-multi.c',
        includes = ['src'],
        use      = ['zpset']
    )

    bld.add_post_fun(waf_unit_test.summary)

    texinputs = [
        build_candcount(bld),
        build_phitab_tex(bld)
    ]

    bld(
        features = 'tex',
        type     = 'pdflatex',
        source   = 'ett-delbarhetsproblem.tex',
        outs     = 'pdf',
        deps     = texinputs
    )
