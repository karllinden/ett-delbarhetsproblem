/*
 * dist.h
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef DIST_H
# define DIST_H

# include "attributes.h"

struct dist_s
{
    unsigned        size;
    unsigned long * array;
};
typedef struct dist_s dist_t;

void          dist_init  (dist_t * dist);
void          dist_deinit(dist_t * dist);
int           dist_count (dist_t * dist,       unsigned index);
unsigned long dist_get   (const dist_t * dist, unsigned index) PURE;
int           dist_print (const dist_t * dist,
                          int            print_total,
                          const char *   indexlabel,
                          const char *   countlabel);

#endif /* !DIST_H */
