/*
 * phitab.c
 *
 * Copyright (C) 2016 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>

#include <gop.h>

#include "phi.h"
#include "print-error.h"
#include "zpset.h"
#include "zpset-io.h"

int
main(int argc, char ** const argv)
{
    int          exit_status = EXIT_FAILURE;
    zpset_t      phi;
    FILE *       file = stdout;
    unsigned int c;

    unsigned int columns = 2;
    const char * output  = NULL;
    zpset_elm_t  nmin    = 1;
    zpset_elm_t  nmax    = 1;
    zpset_elm_t  nstep   = 1;

    zpset_iter_t iter;
    zpset_elm_t  m;

    zpset_init(&phi);

    const gop_option_t options[] = {
        {"columns", 'c', GOP_UNSIGNED_INT, &columns, NULL,
            "columns in table", "COLUMNS"},
        {"output", 'o', GOP_STRING, &output, NULL, "output file",
            "FILENAME"},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL)
    {
        goto error;
    }
    gop_add_usage(gop, "[N-MIN] N-MAX [N-STEP]");
    gop_add_table(gop, "Program options:", options);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (argc < 2)
    {
        print_error("too few arguments");
        goto error;
    }
    else if (argc == 2)
    {
        nmax = zpset_strtoelm(argv[1]);
    }
    else if (argc >= 3)
    {
        nmin = zpset_strtoelm(argv[1]);
        nmax = zpset_strtoelm(argv[2]);
        if (argc == 4)
        {
            nstep = zpset_strtoelm(argv[3]);
        }
        else
        {
            print_error("too many arguments");
            goto error;
        }
    }
    if (nmin == 0 || nmax == 0 || nstep == 0)
    {
        goto error;
    }

    if (columns == 0)
    {
        print_error("colmns = 0 is not allowed");
        goto error;
    }

    if (output != NULL)
    {
        file = fopen(output, "w");
        if (file == NULL)
        {
            file = stdout;
            print_error_errno("could not open %s for writing", output);
            goto error;
        }
    }

    fputs("\\begin{table}[H]\n"
          "  \\centering\n"
          "  \\begin{tabular}{",
          file);
    for (c = 0; c < columns; ++c)
    {
        fputc('l', file);
    }
    fputs("}\n", file);

    c = 0;
    fputs("    ", file);
    for (zpset_elm_t n = nmin; n <= nmax; n += nstep)
    {
        if (c == columns)
        {
            c = 0;
            fputs(" \\\\\n", file);
            fputs("    ", file);
        }
        else if (c != 0)
        {
            fputs(" & ", file);
        }

        if (phi_create(&phi, n))
        {
            goto error;
        }

        fprintf(file, "\\(\\Phi_{%u} = \\{", n);
        zpset_iter_init(&iter, &phi);
        fprintf(file, "%u", zpset_iter(&iter));
        while ((m = zpset_iter(&iter)) != 0)
        {
            fprintf(file, ", %u", m);
        }
        fputs("\\}\\)", file);

        c++;
    }

    fputs("\n"
          "  \\end{tabular}\n"
          "\\end{table}\n",
          file);

    exit_status = EXIT_SUCCESS;
error:
    if (file != stdout)
    {
        if (fclose(file))
        {
            print_error_errno("could not close file %s", output);
            exit_status = EXIT_FAILURE;
        }
    }
    zpset_deinit(&phi);
    return exit_status;
}
