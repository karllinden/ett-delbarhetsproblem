/*
 * time.c
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>

#include <gop.h>

#include "alg.h"
#include "zpset-io.h"

int
main(int argc, char ** const argv)
{
    int exit_status = EXIT_FAILURE;
    unsigned runs = 10;

    const gop_option_t options[] = {
        {"runs", 'r', GOP_UNSIGNED_INT, &runs, NULL, "runs for each n-value",
            NULL},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL)
    {
        goto error;
    }
    gop_add_usage(gop, "N-VALUES...");
    gop_add_table(gop, "Program options:", options);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    for (char ** argp = argv + 1; *argp != NULL; ++argp)
    {
        zpset_elm_t n;
        alg_data_t  data;

        n = zpset_strtoelm(*argp);
        if (n == 0)
        {
            goto error;
        }

        printf("%u:", n);
        for (unsigned r = 0; r < runs; ++r)
        {
            alg_data_zero(&data);
            data.n = n;
            data.time = 1;

            if (alg_run(&data))
            {
                goto error;
            }
            printf(" %ld", alg_time(&data));
        }
        putchar('\n');
    }

    exit_status = EXIT_SUCCESS;
error:
    return exit_status;
}
