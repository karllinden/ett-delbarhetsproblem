/*
 * phi.c
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#include <assert.h>
#include <stddef.h>

#include "phi.h"
#include "zpset.h"

int
phi_create(zpset_t * phi,
           zpset_elm_t n)
{
    int retval = 1;

    assert(n > 0);

    zpset_empty(phi);
    n = n - 1 + (n % 2);
    if (zpset_add(phi, n))
    {
        goto error;
    }

    /* Add as many odd numbers as possible. */
    while (n > 1)
    {
        n -= 2;
        if (!zpset_contains(phi, 3*n))
        {
            if (zpset_add(phi, n))
            {
                goto error;
            }
        }
        else
        {
            break;
        }
    }

    /* n is now the highest odd number that could not be added or 1.
     * The highest even number that can be added is n - 1, as long as it
     * is not zero. */
    n--;
    if (n > 0)
    {
        /* n is atleast 2 so add it and continue adding even numbers as
         * long as it is possible. */
        zpset_add(phi, n);
        while ((n -= 2) > 0 && !zpset_contains(phi, 2*n))
        {
            if (zpset_add(phi, n))
            {
                goto error;
            }
        }
    }

    retval = 0;
error:
    return retval;
}

zpset_t *
phi_new(zpset_elm_t n)
{
    zpset_t * phi = zpset_new();
    if (phi == NULL)
    {
        goto error;
    }

    if (phi_create(phi, n))
    {
        goto error;
    }

    return phi;
error:
    zpset_unref(phi);
    return NULL;
}
