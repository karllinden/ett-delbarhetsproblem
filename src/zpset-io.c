/*
 * zpset-io.c
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <errno.h>
#include <stdlib.h>

#include "digits.h"
#include "print-error.h"
#include "table.h"
#include "zpset.h"
#include "zpset-io.h"

zpset_elm_t
zpset_strtoelm(const char * str)
{
    zpset_elm_t elm;
    char * endptr;

    errno = 0;
    elm = (zpset_elm_t)strtoul(str, &endptr, 0);
    if (errno != 0)
    {
        print_error_errno("could not convert %s to an positive integer",
                          str);
        return 0;
    }
    else if (*endptr != '\0')
    {
        print_error("could not convert %s to an positive integer", str);
        return 0;
    }
    else if (elm == 0)
    {
        print_error("0 is not a positive integer");
        return 0;
    }

    return elm;
}

void
zpset_print_elm_rjust(zpset_elm_t elm,
                      unsigned int width)
{
    unsigned int d = digits(elm);
    for (; d < width; ++d)
    {
        putchar(' ');
    }
    printf("%u", elm);
    return;
}

static int
zpset_print_nonempty(const zpset_t * zpset,
                     const char *    label,
                     zpset_elm_t     count)
{
    int          retval = 1;
    table_t *    table;
    unsigned     column;
    zpset_iter_t iter;
    zpset_elm_t  elm;

    column = 0;
    table = table_new(1, count, label);
    if (!table)
    {
        goto error;
    }

    zpset_iter_init(&iter, zpset);
    while ((elm = zpset_iter(&iter)))
    {
        table_entry(table, 0, column, elm);
        column++;
    }

    if (table_print(table))
    {
        goto error;
    }

    retval = 0;
error:
    table_destroy(table);
    return retval;
}

int
zpset_print(const zpset_t * zpset, const char * label)
{
    zpset_elm_t count = zpset_count(zpset);
    if (count)
    {
        return zpset_print_nonempty(zpset, label, count);
    }
    else
    {
        if (label)
        {
            printf("%s: (empty)\n", label);
        }
        else
        {
            puts("(empty)");
        }
        return 0;
    }
}
