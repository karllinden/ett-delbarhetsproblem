/*
 * alg.h
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef ALG_H
# define ALG_H

# include <string.h>
# include <time.h>

# include "attributes.h"
# include "dist.h"
# include "zpset.h"
# include "zpset-list.h"

struct alg_data_s
{
    zpset_elm_t     n;
    zpset_elm_t     maxsize;
    zpset_list_t *  all;
    zpset_list_t *  max;
    zpset_t *       complement;
    zpset_t *       intersect;
    dist_t *        dist;

    int             time;
    struct timespec t1;
    struct timespec t2;
};
typedef struct alg_data_s alg_data_t;

#define alg_data_zero(data) memset(data, 0, sizeof(alg_data_t));

/* Before running the algorithm alg_begin must be called. As soon as the
 * algorithm is done alg_end should be called. */
int  alg_begin        (alg_data_t * data);
int  alg_end          (alg_data_t * data);

/* Get the time it took to run the algorithm. */
long alg_time         (const alg_data_t * data) PURE;

/* Updates the maximum candidate size and clears neccessary data structures. */
int  alg_update_max   (alg_data_t * data,
                       zpset_elm_t count);

/* Registers a candidate in the algorithm data. If count is non-zero it is taken
 * to be the number of elements in the candidate. None of the arguments may be
 * NULL. The candidate will NOT be copied into the data, but it must be
 * referencable (so that zpset_ref works). Hence, care must be taken when
 * calling this function, both since the candidate must be allocated on the heap
 * and not be modified after this call (since that would ruin the data). */
int  alg_register     (alg_data_t * data,
                       zpset_t *    cand,
                       zpset_elm_t  count);

/* Registers a candidate in the algorithm data. If count is non-zero it is taken
 * to be the number of elements in the candidate. None of the arguments may be
 * NULL. The candidate will be copied into the data. */
int  alg_register_copy(alg_data_t *    data,
                       const zpset_t * cand,
                       zpset_elm_t     count);

/* Function that each algorithm should implement. */
int  alg_run          (alg_data_t * data);

#endif /* ALG_H */
