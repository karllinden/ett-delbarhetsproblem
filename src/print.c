/*
 * print.c
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

/* FIXME: Parallelization. */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <stdarg.h>

#include <gop.h>

#include "alg.h"
#include "attributes.h"
#include "digits.h"
#include "print-error.h"
#include "zpset.h"
#include "zpset-io.h"

struct summary_s
{
    alg_data_t   data;
    zpset_list_t all;
    zpset_list_t max;
    zpset_t      complement;
    zpset_t      intersect;
    dist_t       dist;
};
typedef struct summary_s summary_t;

static int do_all = 0;
static int do_max = 0;
static int do_complement = 0;
static int do_intersect = 0;
static int do_maxsize = 0;
static int do_weight = 0;
static int do_dist = 0;
static int do_time = 0;

static int has_printed_header = 0;

static void PRINTF(1, 3)
print_header(const char * fmt, const char c, ...)
{
    va_list ap;
    int size;

    if (has_printed_header)
    {
        putchar('\n');
    }
    else
    {
        has_printed_header = 1;
    }

    va_start(ap, c);
    size = vsnprintf(NULL, 0, fmt, ap);
    va_end(ap);

    for (int i = 0; i < size; ++i)
    {
        putchar(c);
    }
    putchar('\n');

    va_start(ap, c);
    vprintf(fmt, ap);
    va_end(ap);
    putchar('\n');

    for (int i = 0; i < size; ++i)
    {
        putchar(c);
    }
    putchar('\n');

    return;
}

static int
print_zpset_list(const zpset_list_t * list)
{
    int           retval = 1;
    unsigned      digs;
    char *        label;
    unsigned long count;

    digs = digits(zpset_list_count(list));
    if (!digs)
    {
        puts("(nothing)");
        return 0;
    }

    label = malloc(digs + 1);
    if (!label)
    {
        goto error;
    }

    count = 1;
    for (const zpset_list_link_t * link = zpset_list_first(list);
         link != NULL;
         link = zpset_list_link_next(link))
    {
        zpset_t * zpset = zpset_list_link_zpset(link);
        unsigned  d     = digits(count);
        unsigned  pad   = digs - d;

        memset(label, ' ', pad);
        snprintf(label + pad, d + 1, "%lu", count);
        count++;

        if (zpset_print(zpset, label))
        {
            goto error;
        }
    }

    retval = 0;
error:
    free(label);
    return retval;
}

static int
weight_compute(dist_t * weight, const zpset_list_t * list)
{
    const zpset_t * zpset;
    zpset_iter_t    iter;
    zpset_elm_t     elm;

    for (const zpset_list_link_t * link = zpset_list_first(list);
         link != NULL;
         link = zpset_list_link_next(link))
    {
        zpset = zpset_list_link_zpset(link);
        zpset_iter_init(&iter, zpset);
        while ((elm = zpset_iter(&iter)))
        {
            if (dist_count(weight, elm))
            {
                return 1;
            }
        }
    }

    return 0;
}

static int
weight_print(const summary_t * s)
{
    int    retval = 1;
    dist_t weight;
    dist_init(&weight);
    if (weight_compute(&weight, &s->max))
    {
        goto error;
    }
    print_header("Weight distribution", '-');
    if (dist_print(&weight, 0, "elem", "weight"))
    {
        goto error;
    }
    retval = 0;
error:
    dist_deinit(&weight);
    return retval;
}

static void
summary_init(summary_t * s, zpset_elm_t n)
{
    alg_data_zero(&s->data);
    s->data.n = n;
    if (do_all)
    {
        zpset_list_init(&s->all);
        s->data.all = &s->all;
    }
    if (do_max)
    {
        zpset_list_init(&s->max);
        s->data.max = &s->max;
    }
    if (do_complement)
    {
        zpset_init(&s->complement);
        s->data.complement = &s->complement;
    }
    if (do_intersect)
    {
        zpset_init(&s->intersect);
        s->data.intersect = &s->intersect;
    }
    if (do_maxsize)
    {
        s->data.maxsize = 1;
    }
    if (do_dist)
    {
        dist_init(&s->dist);
        s->data.dist = &s->dist;
    }
    if (do_time)
    {
        s->data.time = 1;
    }
    return;
}

static void
summary_deinit(summary_t * s)
{
    if (do_all)
    {
        zpset_list_deinit(&s->all);
    }
    if (do_max)
    {
        zpset_list_deinit(&s->max);
    }
    if (do_complement)
    {
        zpset_deinit(&s->complement);
    }
    if (do_intersect)
    {
        zpset_deinit(&s->intersect);
    }
    if (do_dist)
    {
        dist_deinit(&s->dist);
    }
    return;
}

static int
summary_print(const summary_t * s)
{
    int retval = 0;

    print_header("Summary for n = %u", '=', s->data.n);
    if (do_all)
    {
        print_header("All candidates", '-');
        print_zpset_list(&s->all);
    }
    if (do_max)
    {
        print_header("Maximum candidates", '-');
        print_zpset_list(&s->max);
    }
    if (do_maxsize)
    {
        printf("\nNumber of elements in maximal candidates: %u\n",
               s->data.maxsize);
    }
    if (do_complement)
    {
        print_header("Elements not in any maximal candidate", '-');
        if (zpset_print(&s->complement, NULL))
        {
            goto error;
        }
    }
    if (do_intersect)
    {
        print_header("Elements common for all maximal candidates", '-');
        if (zpset_print(&s->intersect, NULL))
        {
            goto error;
        }
    }
    if (do_weight)
    {
        if (weight_print(s))
        {
            goto error;
        }
    }
    if (do_dist)
    {
        print_header("Candidate distribution", '-');
        if (dist_print(&s->dist, 1, "elems", "cands"))
        {
            goto error;
        }

    }
    if (do_time)
    {
        long nsec = alg_time(&s->data);
        printf("\nComputation time: %ld nsec\n", nsec);
    }

    retval = 0;
error:
    return retval;
}

static int
run_alg(zpset_elm_t n)
{
    int retval = 1;
    summary_t s;
    summary_init(&s, n);
    if (alg_run(&s.data))
    {
        goto error;
    }
    if (summary_print(&s))
    {
        goto error;
    }
    retval = 0;
error:
    summary_deinit(&s);
    return retval;
}

int
main(int argc, char ** const argv)
{
    int exit_status = EXIT_FAILURE;

    const gop_option_t options[] = {
        {"all", 'a', GOP_NONE, &do_all, NULL, "print all candidates",
            NULL},
        {"max", 'm', GOP_NONE, &do_max, NULL,
            "print maximal candidates", NULL},
        {"complement", 'c', GOP_NONE, &do_complement, NULL,
            "print elements that are not in any maximal candidate",
            NULL},
        {"intersect", 'i', GOP_NONE, &do_intersect, NULL,
            "print intersection of maximal candidates", NULL},
        {"maxsize", 's', GOP_NONE, &do_maxsize, NULL,
            "print size of maximal candidates", NULL},
        {"weights", 'w', GOP_NONE, &do_weight, NULL,
            "print elements of maximal candidates versus how often "
            "they occur (the weight)", NULL},
        {"dist", 'd', GOP_NONE, &do_dist, NULL,
            "print distribution of number of candidates with respect "
            "to number of elements", NULL},
        {"time", 't', GOP_NONE, &do_time, NULL, "print computation time", NULL},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL)
    {
        goto error;
    }
    gop_add_usage(gop, "N-VALUES...");
    gop_add_table(gop, "Program options:", options);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    for (char ** argp = argv + 1; *argp != NULL; ++argp)
    {
        zpset_elm_t n = zpset_strtoelm(*argp);
        if (n == 0)
        {
            goto error;
        }

        /* Print some nice spacing. */
        if (argp > argv + 1)
        {
            putchar('\n');
        }
        if (run_alg(n))
        {
            goto error;
        }
    }

    exit_status = EXIT_SUCCESS;
error:
    return exit_status;
}
