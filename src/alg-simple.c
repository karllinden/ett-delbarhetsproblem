/*
 * alg-simple.c
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>

#include "alg.h"
#include "print-error.h"
#include "zpset.h"

struct stack_s
{
    zpset_t *    cand;
    zpset_t *    remain;
    zpset_iter_t iter;
};
typedef struct stack_s stack_t;

int
alg_run(alg_data_t * data)
{
    int         retval = 1;
    stack_t *   stack  = NULL;
    unsigned    size   = 0;
    unsigned    m;
    zpset_elm_t r;

    zpset_t *      cand;
    zpset_t *      remain;
    zpset_iter_t * iter;

    if (data->n == 0)
    {
        print_error("0 is not a positive number");
        return 1;
    }

    if (alg_begin(data))
    {
        goto error;
    }

    stack = malloc(sizeof(stack_t));
    if (!stack)
    {
        print_error_errno("could not allocate memory");
        goto error;
    }
    size = 1;

    stack[0].cand   = zpset_new();
    stack[0].remain = zpset_new_full(data->n);
    cand   = stack[0].cand;
    remain = stack[0].remain;
    if (!cand || !remain)
    {
        goto error;
    }

    iter   = &stack[0].iter;
    zpset_iter_init(iter, remain);

    if (alg_register(data, cand, 0))
    {
        goto error;
    }

    m = 0;
    while (1)
    {
        if ((r = zpset_iter(iter)))
        {
            m++;

            if (m < size)
            {
                if (zpset_cpy(stack[m].cand, cand) ||
                    zpset_cpy(stack[m].remain, remain))
                {
                    goto error;
                }
            }
            else
            {
                unsigned  new_size;
                stack_t * new_stack;

                new_size = m + 1;
                new_stack = realloc(stack, new_size * sizeof(stack_t));
                if (!new_stack)
                {
                    goto error;
                }
                stack = new_stack;
                size  = new_size;
                stack[m].cand   = zpset_dup(cand);
                stack[m].remain = zpset_dup(remain);
                if (!stack[m].cand || !stack[m].remain)
                {
                    goto error;
                }
            }
            cand   = stack[m].cand;
            remain = stack[m].remain;

            if (zpset_add(cand, r))
            {
                goto error;
            }
            zpset_remove_less(remain, r + 2);
            zpset_remove_multi(remain, r);

            if (alg_register_copy(data, cand, m))
            {
                goto error;
            }

            iter = &stack[m].iter;
            zpset_iter_init(iter, remain);
        }
        else if (m)
        {
            m--;
            cand   = stack[m].cand;
            remain = stack[m].remain;
            iter   = &stack[m].iter;
        }
        else
        {
            break;
        }
    }

    if (alg_end(data))
    {
        goto error;
    }

    retval = 0;
error:
    for (size_t i = 0; i < size; ++i)
    {
        zpset_unref(stack[i].cand);
        zpset_unref(stack[i].remain);
    }
    free(stack);
    return retval;
}
