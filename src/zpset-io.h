/*
 * zpset-io.h
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef ZPSET_IO_H
# define ZPSET_IO_H

# include "attributes.h"
# include "zpset.h"

unsigned int zpset_elm_digits     (zpset_elm_t elm)  CONST;

/* Converts a string to a zpset element. Returns 0 on error. */
zpset_elm_t  zpset_strtoelm       (const char * str);

void         zpset_print_elm_rjust(zpset_elm_t elm,
                                   unsigned int width);
int          zpset_print          (const zpset_t * zpset,
                                   const char * label);

#endif /* !ZPSET_IO_H */
