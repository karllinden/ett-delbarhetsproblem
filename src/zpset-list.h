/*
 * zpset-list.h
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef ZPSET_LIST_H
# define ZPSET_LIST_H

# include "zpset.h"

struct zpset_list_link_s
{
    zpset_t * zpset;
    struct zpset_list_link_s * prev;
    struct zpset_list_link_s * next;
};
typedef struct zpset_list_link_s zpset_list_link_t;

struct zpset_list_s
{
    unsigned long       count;
    zpset_list_link_t * first;
    zpset_list_link_t * last;
};
typedef struct zpset_list_s zpset_list_t;

#define zpset_list_count(list)      ((list)->count)
#define zpset_list_first(list)      ((list)->first)
#define zpset_list_link_next(link)  ((link)->next)
#define zpset_list_link_zpset(link) ((link)->zpset)

void      zpset_list_init      (zpset_list_t * list);
void      zpset_list_deinit    (zpset_list_t * list);
void      zpset_list_clear     (zpset_list_t * list);
int       zpset_list_append    (zpset_list_t * list,
                                zpset_t * zpset);
int       zpset_list_prepend   (zpset_list_t * list,
                                zpset_t * zpset);
zpset_t * zpset_list_pull_first(zpset_list_t * list);
zpset_t * zpset_list_pull_last (zpset_list_t * list);


#endif /* !ZPSET_LIST_H */
