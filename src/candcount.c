/*
 * candcount.c
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>

#include <gop.h>

#include "alg.h"
#include "print-error.h"
#include "zpset.h"
#include "zpset-io.h"

int
main(int argc, char ** const argv)
{
    int           exit_status = EXIT_FAILURE;
    char *        output      = NULL;
    FILE *        file        = NULL;
    zpset_list_t  all;
    alg_data_t    data;
    zpset_elm_t   n;
    unsigned long count;
    zpset_list_init(&all);

    const gop_option_t options[] = {
        {"output", 'o', GOP_STRING, &output, NULL,
            "output file", "OUTPUT"},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL)
    {
        goto error;
    }
    gop_add_usage(gop, "N-VALUES...");
    gop_add_table(gop, "Program options:", options);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (output != NULL)
    {
        file = fopen(output, "w");
        if (!file)
        {
            print_error_errno("could not open %s for writing", output);
            goto error;
        }
    }
    else
    {
        file = stdout;
    }

    for (char ** argp = argv + 1; *argp != NULL; ++argp)
    {
        n = zpset_strtoelm(*argp);
        if (n == 0)
        {
            goto error;
        }

        alg_data_zero(&data);
        data.n = n;
        data.all = &all;
        if (alg_run(&data))
        {
            goto error;
        }
        count = zpset_list_count(&all);

        fprintf(file, "%u %lu\n", n, count);
    }

    exit_status = EXIT_SUCCESS;
error:
    if (file && file != stdout)
    {
        if (fclose(file))
        {
            print_error_errno("could not close file");
            exit_status = EXIT_FAILURE;
        }
    }
    zpset_list_deinit(&all);
    return exit_status;
}
