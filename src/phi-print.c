/*
 * print-phi.c
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>

#include <gop.h>

#include "phi.h"
#include "zpset.h"
#include "zpset-io.h"

int
main(int argc, char ** const argv)
{
    int         exit_status = EXIT_FAILURE;
    zpset_t     phi;
    zpset_elm_t n;
    int         spacing = 0;

    zpset_init(&phi);

    gop_t * const gop = gop_new();
    if (gop == NULL)
    {
        goto error;
    }
    gop_add_usage(gop, "N-VALUES...");
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    for (char ** argp = argv + 1; *argp != NULL; ++argp)
    {
        if (spacing)
        {
            putchar('\n');
        }
        else
        {
            spacing = 1;
        }

        n = zpset_strtoelm(*argp);
        if (n == 0)
        {
            goto error;
        }

        if (phi_create(&phi, n))
        {
            goto error;
        }

        if (zpset_print(&phi, NULL))
        {
            goto error;
        }
    }

    exit_status = EXIT_SUCCESS;
error:
    zpset_deinit(&phi);
    return exit_status;
}
