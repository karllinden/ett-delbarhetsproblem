/*
 * zpset-list.c
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <stdlib.h>

#include "print-error.h"
#include "zpset.h"
#include "zpset-list.h"

static zpset_list_link_t *
zpset_list_link_new(zpset_t * zpset)
{
    zpset_list_link_t * link = malloc(sizeof(zpset_list_link_t));
    if (link == NULL)
    {
        print_error("could not allocate memory");
        return NULL;
    }

    zpset_ref(zpset);
    link->zpset = zpset;
    link->prev = NULL;
    link->next = NULL;

    return link;
}

static void
zpset_list_link_destroy(zpset_list_link_t * link)
{
    zpset_unref(link->zpset);
    free(link);
    return;
}

static void
zpset_list_link_unlink(zpset_list_t * list,
                       zpset_list_link_t * link)
{
    zpset_list_link_t * prev = link->prev;
    zpset_list_link_t * next = link->next;

    if (prev != NULL)
    {
        prev->next = next;
    }
    else
    {
        assert(list->first == link);
        list->first = next;
    }

    if (next != NULL)
    {
        next->prev = prev;
    }
    else
    {
        assert(list->last == link);
        list->last = prev;
    }

    return;
}

static void
zpset_list_zero(zpset_list_t * list)
{
    list->count = 0;
    list->first = NULL;
    list->last  = NULL;
    return;
}

void
zpset_list_init(zpset_list_t * list)
{
    zpset_list_zero(list);
    return;
}

void
zpset_list_deinit(zpset_list_t * list)
{
    zpset_list_clear(list);
    return;
}

void
zpset_list_clear(zpset_list_t * list)
{
    zpset_list_link_t * link = list->first;
    zpset_list_link_t * next;
    while (link != NULL)
    {
        next = link->next;
        zpset_list_link_destroy(link);
        link = next;
    }
    zpset_list_zero(list);
    return;
}

int
zpset_list_append(zpset_list_t * list,
                  zpset_t * zpset)
{
    zpset_list_link_t * prev = list->last;
    zpset_list_link_t * link = zpset_list_link_new(zpset);
    if (link == NULL)
    {
        return 1;
    }

    list->count++;

    link->prev = prev;
    list->last = link;
    if (prev != NULL)
    {
        assert(list->first != NULL);
        prev->next = link;
    }
    else
    {
        assert(list->first == NULL);
        list->first = link;
    }

    return 0;
}

int
zpset_list_prepend(zpset_list_t * list,
                   zpset_t * zpset)
{
    zpset_list_link_t * next = list->first;
    zpset_list_link_t * link = zpset_list_link_new(zpset);
    if (link == NULL)
    {
        return 1;
    }

    list->count++;

    link->next = next;
    list->first = link;
    if (next != NULL)
    {
        assert(list->last != NULL);
        next->prev = link;
    }
    else
    {
        assert(list->last == NULL);
        list->last = link;
    }

    return 0;
}

static zpset_t *
zpset_list_pull(zpset_list_t * list,
                zpset_list_link_t * link)
{
    zpset_t * zpset = NULL;
    if (link != NULL)
    {
        list->count--;
        zpset_list_link_unlink(list, link);
        zpset = link->zpset;
        zpset_ref(zpset);
        zpset_list_link_destroy(link);
    }
    return zpset;
}

zpset_t *
zpset_list_pull_first(zpset_list_t * list)
{
    return zpset_list_pull(list, list->first);
}

zpset_t *
zpset_list_pull_last(zpset_list_t * list)
{
    return zpset_list_pull(list, list->last);
}
