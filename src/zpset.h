/*
 * zpset.h
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef ZPSET_H
# define ZPSET_H

# include "attributes.h"

typedef unsigned int   zpset_elm_t;
typedef unsigned long  zpset_dta_t;
typedef zpset_elm_t    zpset_idx_t;

struct zpset_s {
    zpset_idx_t   size;
    zpset_dta_t * buf;

    /* Reference count. If it is set to a negative value, the zpset may
     * not be referenced or dereferenced. */
    int           ref;
};
typedef struct zpset_s zpset_t;

struct zpset_iter_s
{
    const zpset_t * zpset;
    zpset_idx_t     idx;
    zpset_dta_t     dta; /* consumed dtas */
};
typedef struct zpset_iter_s zpset_iter_t;

typedef int (zpset_func_t)(zpset_elm_t elm, void * data);

/* Returns the index of the minumum and maximum bits set in dta,
 * respectively. There must be bits set in bitmask or the behaviour is
 * undefined. */
unsigned int zpset_dta_min           (zpset_dta_t bitmask)   CONST;
unsigned int zpset_dta_max           (zpset_dta_t bitmask)   CONST;

/* These functions are used to initialize and deinitialize that may
 * neither be referenced not dereferenced. */
void         zpset_init              (zpset_t * zpset);
int          zpset_init_empty        (zpset_t * zpset,
                                      zpset_elm_t max);
int          zpset_init_full         (zpset_t * zpset,
                                      zpset_elm_t max);
void         zpset_deinit            (zpset_t * zpset);

/* These functions create a new zpset that may be referenced and
 * unreferenced with zpset_ref and zpset_unref respectively. Initially
 * the sets have reference count 1. */
zpset_t *    zpset_new               (void);
zpset_t *    zpset_new_empty         (zpset_elm_t max);
zpset_t *    zpset_new_full          (zpset_elm_t max);

/* These functions increase and decrease the reference count of a zpset.
 * If the reference count drops to 0 the zpset is destroyed. */
void         zpset_ref               (zpset_t * zpset);
void         zpset_unref             (zpset_t * zpset);

/* This function copies the content of src into dest, which must have
 * been initialized prior to a call to this function. */
int          zpset_cpy               (zpset_t * dest,
                                      const zpset_t * src);

/* Duplicates the set, i.e. creates a copy. */
zpset_t *    zpset_dup               (const zpset_t * zpset);

int          zpset_is_empty          (const zpset_t * zpset) PURE;
zpset_elm_t  zpset_count             (const zpset_t * zpset) PURE;
zpset_elm_t  zpset_min               (const zpset_t * zpset) PURE;
zpset_elm_t  zpset_max               (const zpset_t * zpset) PURE;

int          zpset_contains          (const zpset_t * zpset,
                                      zpset_elm_t elm)       PURE;

int          zpset_add               (zpset_t * zpset,
                                      zpset_elm_t elm);
void         zpset_remove            (zpset_t * zpset,
                                      zpset_elm_t elm);

/* Remove all elements less than elm. */
void         zpset_remove_less       (zpset_t * zpset,
                                      zpset_elm_t elm);

/* Remove all multiples of elm. */
void         zpset_remove_multi      (zpset_t * zpset,
                                      zpset_elm_t elm);

/* Remove all multiples of elm and return non-zero if any elements were
 * removed. */
int          zpset_remove_multi_check(zpset_t * zpset,
                                      zpset_elm_t elm);

void         zpset_empty             (zpset_t * zpset);
int          zpset_fill              (zpset_t * zpset,
                                      zpset_elm_t max);

void         zpset_intersect_here    (zpset_t * a,
                                      const zpset_t * b);
zpset_t *    zpset_intersect         (const zpset_t * a,
                                      const zpset_t * b);

void         zpset_minus_here        (zpset_t * a,
                                      const zpset_t * b);
zpset_t *    zpset_minus             (const zpset_t * a,
                                      const zpset_t * b);

int          zpset_union_here        (zpset_t * a,
                                      const zpset_t * b);
zpset_t *    zpset_union             (const zpset_t * a,
                                      const zpset_t * b);

void         zpset_iter_init         (zpset_iter_t * iter,
                                      const zpset_t * zpset);
zpset_elm_t  zpset_iter              (zpset_iter_t * iter);

#endif /* !ZPSET_H */
