/*
 * table.h
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef TABLE_H
# define TABLE_H

struct table_s
{
    unsigned n_rows;
    unsigned n_cols;

    unsigned maxlabel; /* len of longest label */
    char **  labels;

    /* An array with all the columns in order. The index of (row,column)
     * is colums * n_rows + row. */
    unsigned long entries[];
};
typedef struct table_s table_t;

/* Create a new table. The variable arguments are the labels of each
 * row, which may not be left out! If a row without label is desired
 * pass NULL or "". */
table_t * table_new    (unsigned n_rows,
                        unsigned n_columns,
                        ...);
void      table_destroy(table_t * table);
void      table_entry  (table_t * table,
                        unsigned row,
                        unsigned column,
                        unsigned long entry);
int       table_print  (const table_t * table);

#endif /* !TABLE_H */
