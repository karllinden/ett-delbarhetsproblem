/*
 * zpset.c
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "print-error.h"
#include "zpset.h"

#define ZPSET_ELM_PER_DTA (8 * sizeof(zpset_dta_t))

static unsigned CONST
zpset_dta_count(zpset_dta_t x)
{
#if HAVE___BUILTIN_POPCOUNTL
    return (unsigned)__builtin_popcountl(x);
#elif SIZEOF_UNSIGNED_LONG == 8
    /* Code found on Wikipedia. */
    /*
     * 0101...
     * 00110011...
     * 4 zeroes, 4 ones, ...
     * sum of 256^k, k = 0,1,2,...
     */
    const zpset_dta_t m1  = 0x5555555555555555;
    const zpset_dta_t m2  = 0x3333333333333333;
    const zpset_dta_t m4  = 0x0f0f0f0f0f0f0f0f;
    const zpset_dta_t h01 = 0x0101010101010101;

    /*
     * 1. put count of each 2 bits into those 2 bits
     * 2. put count of each 4 bits into those 4 bits
     * 3. put count of each 8 bits into those 8 bits
     * 4. returns left 8 bits of x + (x<<8) + (x<<16) + (x<<24) + ...
     */
    x -= (x >> 1) & m1;
    x = (x & m2) + ((x >> 2) & m2);
    x = (x + (x >> 4)) & m4;
    return (unsigned)((x * h01) >> 56);
#else
# error Not implemented
#endif
}

unsigned int CONST
zpset_dta_min(zpset_dta_t bitmask)
{
#if HAVE___BUILTIN_CTZL
    return (unsigned int)__builtin_ctzl(bitmask);
#else
    unsigned int index = 0;
    unsigned int size = sizeof(zpset_dta_t) << 3;
    zpset_dta_t mask = ~0lu;

    assert(bitmask);

    /* This algorithm repeatedly tests if the lower part of the bitmask
     * does not have any bits by ANDing them with a decreasing mask. If
     * this is the case the bitmask is shifted downwards to contain a
     * set bit and the index is incremented. */
    while (size > 1)
    {
        size >>= 1;
        mask >>= size;
        if (!(bitmask & mask))
        {
            index += size;
            bitmask >>= size;
        }
    }

    return index;
#endif
}

unsigned int CONST
zpset_dta_max(zpset_dta_t bitmask)
{
#if HAVE___BUILTIN_CLZL
    return (unsigned int)(sizeof(zpset_dta_t) << 3) -
           (unsigned int)__builtin_clzl(bitmask) - 1;
#else
    unsigned int index = 0;
    unsigned int size = sizeof(zpset_dta_t) << 3;

    assert(bitmask);

    /* This algorithm shifts away the lower half of the remaining
     * bitmask and checks if there is in the upper half by simply
     * checking if the shifted bitmask is non-zero. If this is the case
     * the index must be incremented with the number of removed bits and
     * the bitmask is updated. Otherwise the algorithm can just continue
     * without changing neither bitmask nor index. */
    while (size > 1)
    {
        size >>= 1;
        if (bitmask >> size)
        {
            index += size;
            bitmask >>= size;
        }
    }

    return index;
#endif
}

static void
zpset_elm_to_idx(zpset_elm_t elm,
                 zpset_idx_t * idxp,
                 zpset_dta_t * bitmaskp)
{
    zpset_idx_t idx;
    zpset_dta_t bit;
    zpset_dta_t bitmask;

    /* Zero is not allowed. */
    assert(elm > 0);
    elm--;

    idx = elm / ZPSET_ELM_PER_DTA;
    bit = elm % ZPSET_ELM_PER_DTA;
    bitmask = 1ul << bit;

    *idxp = idx;
    *bitmaskp = bitmask;

    return;
}

static void
zpset_fill_idx(zpset_t * zpset,
               zpset_idx_t idx,
               zpset_dta_t bitmask)
{
    /* Fully fill all dtas except the last one. */
    memset(zpset->buf, ~0, idx * sizeof(zpset_dta_t));

    /*
     * This makes the conversion
     *  ...010...000 -> ...100...000 -> ...011...111
     * where the last one is the bitmask corresponding to all elements
     * in the last idx up to and including this.
     * If bitmask happens to become zero when shifting this is not a
     * problem, since then the conversion is
     *  100...000 -> 000...000 -> 111...111.
     */
    zpset->buf[idx] |= ((bitmask << 1) - 1);
    return;
}

void
zpset_init(zpset_t * zpset)
{
    zpset->size = 0;
    zpset->buf = NULL;
    zpset->ref = -1;
    return;
}

int
zpset_init_empty(zpset_t * zpset,
                 zpset_elm_t max)
{
    zpset_idx_t idx;
    zpset_dta_t bitmask;

    zpset_init(zpset);
    if (max != 0)
    {
        zpset_elm_to_idx(max, &idx, &bitmask);

        zpset->size = idx + 1;
        zpset->buf = calloc(zpset->size, sizeof(zpset_dta_t));
        if (zpset->buf == NULL)
        {
            print_error_errno("could not allocate memory");
            return 1;
        }
    }

    return 0;
}

int
zpset_init_full(zpset_t * zpset,
                zpset_elm_t max)
{
    zpset_init(zpset);
    if (max != 0)
    {
        zpset_idx_t idx;
        zpset_dta_t bitmask;
        zpset_elm_to_idx(max, &idx, &bitmask);

        zpset->size = idx + 1;
        zpset->buf = malloc(zpset->size * sizeof(zpset_dta_t));
        if (zpset->buf == NULL)
        {
            print_error_errno("could not allocate memory");
            return 1;
        }

        /* Zero the last idx, since it will not be overwritten by
         * zpset_fill_idx, but instead ORed. */
        zpset->buf[idx] = 0;
        zpset_fill_idx(zpset, idx, bitmask);
    }
    return 0;
}

void
zpset_deinit(zpset_t * zpset)
{
    assert(zpset->ref < 0);
    free(zpset->buf);
    return;
}

static zpset_t *
zpset_new_alloc(void)
{
    zpset_t * zpset = malloc(sizeof(zpset_t));
    if (zpset == NULL)
    {
        print_error_errno("could not allocate memory");
    }
    return zpset;
}

zpset_t *
zpset_new(void)
{
    zpset_t * zpset = zpset_new_alloc();
    if (zpset != NULL)
    {
        zpset_init(zpset);
        zpset->ref = 1;
    }
    return zpset;
}

zpset_t *
zpset_new_empty(zpset_elm_t max)
{
    zpset_t * zpset = zpset_new_alloc();
    if (zpset == NULL || zpset_init_empty(zpset, max))
    {
        free(zpset);
        return NULL;
    }
    zpset->ref = 1;
    return zpset;
}

zpset_t *
zpset_new_full(zpset_elm_t max)
{
    zpset_t * zpset = zpset_new_alloc();
    if (zpset == NULL || zpset_init_full(zpset, max))
    {
        free(zpset);
        return NULL;
    }
    zpset->ref = 1;
    return zpset;
}

static void
zpset_destroy(zpset_t * zpset)
{
    assert(zpset);
    zpset->ref = -1;
    zpset_deinit(zpset);
    free(zpset);
    return;
}

void
zpset_ref(zpset_t * zpset)
{
    assert(zpset->ref > 0);
    zpset->ref++;
    return;
}

void
zpset_unref(zpset_t * zpset)
{
    if (zpset)
    {
        assert(zpset->ref > 0);
        zpset->ref--;
        if (!zpset->ref)
        {
            zpset_destroy(zpset);
        }
    }
    return;
}

static int
zpset_alloc(zpset_t * zpset,
            zpset_idx_t size)
{
    zpset_dta_t * new;

    if (size <= zpset->size)
    {
        return 0;
    }

    new = realloc(zpset->buf, size * sizeof(zpset_dta_t));
    if (new == NULL)
    {
        print_error_errno("could not allocate memory");
        return 1;
    }

    memset(new + zpset->size,
           0,
           (size - zpset->size) * sizeof(zpset_dta_t));

    zpset->size = size;
    zpset->buf = new;

    return 0;
}

int
zpset_cpy(zpset_t * dest, const zpset_t * src)
{
    if (zpset_alloc(dest, src->size))
    {
        return 1;
    }

    memcpy(dest->buf, src->buf, src->size * sizeof(zpset_dta_t));
    if (src->size < dest->size)
    {
        memset(dest->buf + src->size,
               0,
               (dest->size - src->size) * sizeof(zpset_dta_t));
    }

    return 0;
}

zpset_t *
zpset_dup(const zpset_t * zpset)
{
    zpset_t * cpy;

    cpy = zpset_new_alloc();
    if (cpy == NULL)
    {
        goto error;
    }

    zpset_init_empty(cpy, zpset->size);
    if (zpset_cpy(cpy, zpset))
    {
        goto error;
    }
    cpy->ref = 1;

    return cpy;
error:
    free(cpy);
    return NULL;
}

static zpset_elm_t
zpset_idx_and_bit_to_elm(zpset_idx_t idx,
                         unsigned int bit)
{
    return (zpset_elm_t)(idx * ZPSET_ELM_PER_DTA + bit + 1);
}

int PURE
zpset_is_empty(const zpset_t * zpset)
{
    for (zpset_idx_t idx = 0; idx < zpset->size; ++idx)
    {
        if (zpset->buf[idx])
        {
            return 0;
        }
    }
    return 1;
}

zpset_elm_t PURE
zpset_count(const zpset_t * zpset)
{
    zpset_elm_t cnt = 0;
    for (zpset_idx_t idx = 0; idx < zpset->size; ++idx)
    {
        cnt += zpset_dta_count(zpset->buf[idx]);
    }
    return cnt;
}

zpset_elm_t PURE
zpset_min(const zpset_t * zpset)
{
    for (zpset_idx_t idx = 0; idx < zpset->size; ++idx)
    {
        if (zpset->buf[idx])
        {
            unsigned int bit = zpset_dta_min(zpset->buf[idx]);
            return zpset_idx_and_bit_to_elm(idx, bit);
        }
    }
    return 0;
}

static int
zpset_max_idx_and_bit(const zpset_t * zpset,
                      zpset_idx_t * idxp,
                      unsigned int * bitp)
{
    /* Unsigned integer wrap-around is used here. */
    for (zpset_idx_t idx = (zpset_idx_t)(zpset->size - 1);
         idx < zpset->size;
         --idx)
    {
        if (zpset->buf[idx])
        {
            *idxp = idx;
            *bitp = zpset_dta_max(zpset->buf[idx]);
            return 0;
        }
    }
    return 1;
}

zpset_elm_t PURE
zpset_max(const zpset_t * zpset)
{
    zpset_idx_t idx;
    unsigned int bit;
    if (zpset_max_idx_and_bit(zpset, &idx, &bit))
    {
        return 0;
    }
    else
    {
        return zpset_idx_and_bit_to_elm(idx, bit);
    }
}

int PURE
zpset_contains(const zpset_t * zpset,
               zpset_elm_t elm)
{
    zpset_idx_t idx;
    zpset_dta_t bitmask;
    zpset_elm_to_idx(elm, &idx, &bitmask);
    if (idx < zpset->size)
    {
        return (zpset->buf[idx] & bitmask) != 0;
    }
    else
    {
        return 0;
    }
}

int
zpset_add(zpset_t * zpset,
          zpset_elm_t elm)
{
    zpset_idx_t idx;
    zpset_dta_t bitmask;
    zpset_elm_to_idx(elm, &idx, &bitmask);

    if (zpset_alloc(zpset, idx + 1))
    {
        return 1;
    }

    zpset->buf[idx] |= bitmask;

    return 0;
}

void
zpset_remove(zpset_t * zpset,
             zpset_elm_t elm)
{
    zpset_idx_t idx;
    zpset_dta_t bitmask;
    zpset_elm_to_idx(elm, &idx, &bitmask);

    if (idx < zpset->size)
    {
        zpset->buf[idx] &= ~bitmask;
    }

    return;
}

void
zpset_remove_less(zpset_t * zpset,
                  zpset_elm_t elm)
{
    zpset_idx_t idx;
    zpset_dta_t bitmask;
    zpset_elm_to_idx(elm, &idx, &bitmask);
    memset(zpset->buf, 0, idx * sizeof(zpset_dta_t));

    /* The bitmask is set to the bit corresponding to the element. Like
     * this.
     *   000...010...000
     * If one wants to remove all elements less than this element one
     * simply ANDs the current dta with
     *   111...110...000
     * which can be obtained by first subtracting one, which gives
     *   000...001...111
     * and then taking the complement. */
    zpset->buf[idx] &= ~(bitmask - 1);
    return;
}

void
zpset_remove_multi(zpset_t * zpset,
                   zpset_elm_t e)
{
    /* Wordsize. */
    unsigned w = sizeof(zpset_dta_t) << 3;
    assert(e > 0);

    if (e <= w)
    {
        /* This is the simple case when there will be at least one elm
         * removed from each dta. In this case create an as big as
         * possible bitmask that will be shifted to remove elements from
         * each dta. */

        zpset_dta_t bitmask;
        unsigned bits;
        unsigned mod;

        /*
         * Create the bitmask. Say a word contains 32 bits, and all
         * multiples of 5 should be removed. In that case a bitmask with
         * each fifth bit set to 1 shall be produced. Begin with only
         * the lowest bit set, like this
         *     0000 0000 0000 0000 0000 0000 0000 0001.
         * The number of correct bits in this mask is now 5, since the
         * fifth bit should be set. To set this the mask is shifted 5
         * bits to the left and ORed with the previous to produce the
         * following
         *     0000 0000 0000 0000 0000 0000 0010 0000
         *  OR 0000 0000 0000 0000 0000 0000 0000 0001
         *  ------------------------------------------
         *     0000 0000 0000 0000 0000 0000 0010 0001.
         * Now the 10 first bits are correct. Shifting 10 bits to the
         * left and ORing like before gives
         *     0000 0000 0000 0000 1000 0100 0000 0000
         *  OR 0000 0000 0000 0000 0000 0000 0010 0001
         *  ------------------------------------------
         *     0000 0000 0000 0000 1000 0100 0010 0001,
         * which has 20 correct bits. In the last run the bitmask is as
         * before ORed with itself
         *     0100 0010 0001 0000 0000 0000 0000 0000
         *  OR 0000 0000 0000 0000 1000 0100 0010 0001
         *  ------------------------------------------
         *     0100 0010 0001 0000 1000 0100 0010 0001.
         * Now a bitmask with each fifth bit set to 1 has been acquired
         * using only three OR operations. This is the process written
         * in the following loop (except for an arbitrary wordsizes and
         * multiples, of course).
         */
        bitmask = 1;
        for (unsigned s = e; s < w; s <<= 1)
        {
            bitmask = (bitmask << s) | bitmask;
        }

        /* This is the number of bits to shift the bitmask leftwards in
         * each iteration. Initially this is the element to remove minus
         * one. */
        bits = e - 1;

        /* In fact, the code used in the mod != 0 case can be used also
         * in the mod == 0 case, but splitting the cases saves a some
         * cycles in the mod == 0 case.. */
        mod = w % e;
        if (mod)
        {
            for (zpset_idx_t idx = 0; idx < zpset->size; ++idx)
            {
                zpset->buf[idx] &= ~(bitmask << bits);

                /* The question now is how many bits to shift the
                 * bitmask in the next iteration. If one counts the
                 * zeroes that are shifted out of the bitmask, its
                 * length is a multiple of e. Like in the example above
                 * (but with word size 16 instead) the bitmask is
                 *   (0000) 1000 0100 0010 0001
                 * The actual bitmask is 16 bits, but there are 4
                 * invisible zeroes that is the number of bits to shift
                 * in the next iteration. Say the bitmask was shifted
                 * b_n bits, where b_n < e. Then by considering the
                 * following figure
                 *   b_{n+1} = 1          b_n = 2
                 *      ||                   ||
                 *      \/                   \/
                 *   (0010) 0001 0000 1000 0100
                 *       *  **** **** **** **
                 * One sees that the number of stars is a multiple of
                 * e and that
                 *   b_n + k*e = w + b_{n+1}
                 * where k is an integer and w denotes the wordsize.
                 * Solving for b_{n+1} yields
                 *   b_{n+1} = b_n - w + k*e.
                 * Since b_{n+1} should be the smallest possible
                 * non-negative integer one can take (mod e) and get rid
                 * of the k*e term to get
                 *   b_{n+1} = b_n - w (mod e).
                 * Since b_n < e,
                 *   b_n(mod e) = b_n
                 * w(mod e) was computed above, so there is no need to
                 * compute it again. The subtraction can simply be
                 * carried as long as the result is non-negative and
                 * only in the case where
                 *   w(mod e) > b_n
                 * one has to compensate with an extra e.
                 *
                 * Note that since
                 *   b_0 = e - 1 < e
                 * induction gives that all
                 *   b_n < e
                 * for all n with the above algorithm. */
                if (bits < mod)
                {
                    bits += e;
                }
                bits -= mod;
            }
        }
        else
        {
            /* Since e divides the word size, the bitmask will be
             * shifted the same number of bits each run. Hence, it can
             * be computed in advance. */
            bitmask = ~(bitmask << bits);
            for (zpset_idx_t idx = 0; idx < zpset->size; ++idx)
            {
                zpset->buf[idx] &= bitmask;
            }
        }
    }
    else /* if (e > w) */
    {
        /* In this case, there is no guarantee that each dta will
         * get an element removed, but no dta will be able to get more
         * than one element removed. Thus, there is no reason to compute
         * a bitmask as was done above. */
        zpset_idx_t idxstp;
        unsigned bitstp;
        zpset_idx_t idx;
        unsigned bits;

        idxstp = e / w;
        bitstp = e % w;

        /* Since the division and mod operation already have been
         * performed, the result can be used to avoid the following two
         * commented out operations. */
        /* idx = (e-1) / w; */
        /* bits = (e-1) % w; */
        if (bitstp)
        {
            idx = idxstp;
            bits = bitstp - 1;
        }
        else
        {
            idx = idxstp - 1;
            bits = w - 1;
        }

        while (idx < zpset->size)
        {
            zpset->buf[idx] &= ~(1ul << bits);
            idx += idxstp;
            bits += bitstp;
            if (bits >= w)
            {
                idx++;
                bits -= w;
            }
        }
    }

    return;
}

/* For comments see zpset_remove_multi. This function should look
 * exactly like it, but with the change that it should check if any
 * elements were removed. */
int
zpset_remove_multi_check(zpset_t * zpset,
                        zpset_elm_t e)
{
    unsigned long ret = 0;
    unsigned      w   = sizeof(zpset_dta_t) << 3;
    assert(e > 0);

    if (e <= w)
    {
        zpset_dta_t bitmask;
        unsigned bits;
        unsigned mod;

        bitmask = 1;
        for (unsigned s = e; s < w; s <<= 1)
        {
            bitmask = (bitmask << s) | bitmask;
        }

        bits = e - 1;

        mod = w % e;
        if (mod)
        {
            for (zpset_idx_t idx = 0; idx < zpset->size; ++idx)
            {
                ret |= zpset->buf[idx] & (bitmask << bits);
                zpset->buf[idx] &= ~(bitmask << bits);
                if (bits < mod)
                {
                    bits += e;
                }
                bits -= mod;
            }
        }
        else
        {
            bitmask = (bitmask << bits);
            for (zpset_idx_t idx = 0; idx < zpset->size; ++idx)
            {
                ret |= zpset->buf[idx] & bitmask;
                zpset->buf[idx] &= ~bitmask;
            }
        }
    }
    else /* if (e > w) */
    {
        zpset_idx_t idxstp;
        unsigned bitstp;
        zpset_idx_t idx;
        unsigned bits;

        idxstp = e / w;
        bitstp = e % w;

        if (bitstp)
        {
            idx = idxstp;
            bits = bitstp - 1;
        }
        else
        {
            idx = idxstp - 1;
            bits = w - 1;
        }

        while (idx < zpset->size)
        {
            ret |= zpset->buf[idx] & (1ul << bits);
            zpset->buf[idx] &= ~(1ul << bits);
            idx += idxstp;
            bits += bitstp;
            if (bits >= w)
            {
                idx++;
                bits -= w;
            }
        }
    }

    return ret != 0;
}

void
zpset_empty(zpset_t * zpset)
{
    memset(zpset->buf, 0, zpset->size * sizeof(zpset_dta_t));
    return;
}

int
zpset_fill(zpset_t * zpset,
           zpset_elm_t max)
{
    zpset_idx_t idx;
    zpset_dta_t bitmask;
    zpset_elm_to_idx(max, &idx, &bitmask);

    /* Do not use zpset_alloc here since it unnecessarily zeroes out the
     * newly allocated memory. */
    if (idx >= zpset->size)
    {
        zpset_idx_t   new_size = idx + 1;
        zpset_dta_t * new;

        new = realloc(zpset->buf, new_size * sizeof(zpset_dta_t));
        if (new == NULL)
        {
            print_error_errno("could not allocate memory");
            return 1;
        }

        zpset->size = new_size;
        zpset->buf = new;

        /* Only zero out the last idx, since the other ones will be
         * filled with ones by zpset_fill_idx. */
        zpset->buf[idx] = 0;
    }

    zpset_fill_idx(zpset, idx, bitmask);
    return 0;
}

static void
zpset_intersect_perform(zpset_t * a,
                        const zpset_t * b,
                        zpset_idx_t size)
{
    for (zpset_idx_t idx = 0; idx < size; ++idx)
    {
        a->buf[idx] &= b->buf[idx];
    }
    return;
}

void
zpset_intersect_here(zpset_t * a,
                     const zpset_t * b)
{
    zpset_idx_t size = a->size;
    if (b->size < size)
    {
        size = b->size;
    }

    zpset_intersect_perform(a, b, size);

    if (size < a->size)
    {
        memset(a->buf + size,
               0,
               (a->size - size) * sizeof(zpset_dta_t));
    }

    return;
}

zpset_t *
zpset_intersect(const zpset_t * a,
                const zpset_t * b)
{
    zpset_t * copy;
    const zpset_t * other;
    if (a->size < b->size)
    {
        copy = zpset_dup(a);
        other = b;
    }
    else
    {
        other = a;
        copy = zpset_dup(b);
    }
    if (copy == NULL)
    {
        return NULL;
    }

    zpset_intersect_perform(copy, other, copy->size);

    return copy;
}

void
zpset_minus_here(zpset_t * a,
                 const zpset_t * b)
{
    zpset_idx_t size = a->size;
    if (b->size < size)
    {
        size = b->size;
    }
    for (zpset_idx_t idx = 0; idx < size; ++idx)
    {
        a->buf[idx] &= ~b->buf[idx];
    }
    return;
}

zpset_t *
zpset_minus(const zpset_t * a,
            const zpset_t * b)
{
    zpset_t * dup = zpset_dup(a);
    if (dup == NULL)
    {
        return NULL;
    }
    zpset_minus_here(dup, b);
    return dup;
}

static void
zpset_union_perform(zpset_t * a,
                    const zpset_t * b,
                    zpset_idx_t size)
{
    for (zpset_idx_t idx = 0; idx < size; ++idx)
    {
        a->buf[idx] |= b->buf[idx];
    }
    return;
}

int
zpset_union_here(zpset_t * a,
                 const zpset_t * b)
{
    zpset_idx_t size;
    unsigned int bit;
    if (zpset_max_idx_and_bit(b, &size, &bit))
    {
        return 0;
    }
    size++;
    if (zpset_alloc(a, size))
    {
        return 1;
    }
    zpset_union_perform(a, b, size);
    return 0;
}

zpset_t *
zpset_union(const zpset_t * a,
            const zpset_t * b)
{
    zpset_idx_t amax;
    zpset_idx_t bmax;
    unsigned int abit;
    unsigned int bbit;

    zpset_idx_t size;
    zpset_t * copy;
    const zpset_t * other;

    if (zpset_max_idx_and_bit(a, &amax, &abit))
    {
        return zpset_dup(b);
    }
    if (zpset_max_idx_and_bit(b, &bmax, &bbit))
    {
        return zpset_dup(a);
    }

    if (amax > bmax)
    {
        size = bmax + 1;
        copy = zpset_dup(a);
        other = b;
    }
    else
    {
        size = amax + 1;
        copy = zpset_dup(b);
        other = a;
    }
    if (copy == NULL)
    {
        return NULL;
    }

    zpset_union_perform(copy, other, size);

    return copy;
}

void
zpset_iter_init(zpset_iter_t * iter,
                const zpset_t * zpset)
{
    iter->zpset = zpset;
    iter->idx = 0;
    iter->dta = 0;
    return;
}

zpset_elm_t
zpset_iter(zpset_iter_t * iter)
{
    const zpset_t * zpset = iter->zpset;
    zpset_dta_t     dta;
    zpset_dta_t     bitmask;
    unsigned        bit;
    while (iter->idx < zpset->size)
    {
        dta = zpset->buf[iter->idx] & ~iter->dta;
        if (dta)
        {
            bit = zpset_dta_min(dta);
            bitmask = 1ul << bit;
            iter->dta |= (bitmask << 1) - 1;
            return zpset_idx_and_bit_to_elm(iter->idx, bit);
        }
        iter->idx++;
        iter->dta = 0;
    }
    return 0;
}
