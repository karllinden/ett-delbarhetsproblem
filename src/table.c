/*
 * table.c
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "digits.h"
#include "print-error.h"
#include "table.h"

#define WIDTH 72

table_t *
table_new(unsigned n_rows,
          unsigned n_cols,
          ...)
{
    table_t * table     = NULL;
    unsigned  n_entries = n_rows * n_cols;
    size_t    size      = n_entries * sizeof(unsigned long);
    va_list   ap;
    unsigned  i         = 0; /* iteration variable for labels */

    table = malloc(sizeof(table_t) + size);
    if (!table)
    {
        print_error_errno("could not allocate memory");
        goto error;
    }

    table->n_rows   = n_rows;
    table->n_cols   = n_cols;
    table->maxlabel = 0;
    table->labels   = NULL;
    memset(table->entries, 0, size);

    table->labels = calloc(n_rows, sizeof(char *));
    if (!table->labels)
    {
        print_error_errno("could not allocate memory");
        goto error;
    }

    va_start(ap, n_cols);
    for (i = 0; i < n_rows; ++i)
    {
        const char * label = va_arg(ap, const char *);
        if (label != NULL && *label == '\0')
        {
            label = NULL;
        }

        if (label != NULL)
        {
            unsigned len = (unsigned)strlen(label);
            char * copy;

            if (len > table->maxlabel)
            {
                table->maxlabel = len;
            }

            copy = malloc(len + 1);
            if (!copy)
            {
                print_error_errno("could not allocate memory");
                goto error;
            }
            memcpy(copy, label, len + 1);

            table->labels[i] = copy;
        }
    }
    va_end(ap);

    /* Be economic if all labels were NULL. */
    if (!table->maxlabel)
    {
        free(table->labels);
        table->labels = NULL;
    }

    return table;
error:
    table_destroy(table);
    return NULL;
}

void
table_destroy(table_t * table)
{
    if (table)
    {
        if (table->labels)
        {
            for (unsigned i = 0; i < table->n_rows; ++i)
            {
                free(table->labels[i]);
            }
            free(table->labels);
        }
        free(table);
    }
    return;
}

void
table_entry(table_t * table,
            unsigned row,
            unsigned col,
            unsigned long entry)
{
    if (row >= table->n_rows || col >= table->n_cols)
    {
        print_error("invalid entry");
        abort();
    }
    table->entries[col * table->n_rows + row] = entry;
    return;
}

static void
table_print_label(const table_t * table,
                  unsigned  row)
{
    if (table->maxlabel)
    {
        const char * label = table->labels[row];
        size_t       len   = 0;
        if (label != NULL)
        {
            len = strlen(label);
            fputs(label, stdout);
        }
        for (; len < table->maxlabel; ++len)
        {
            putchar(' ');
        }
        fputs(": ", stdout);
    }
    return;
}

static void
table_print_entry_rjust(const table_t * table,
                        unsigned row,
                        unsigned col,
                        unsigned width)
{
    unsigned long entry = table->entries[col * table->n_rows + row];
    unsigned digs = digits(entry);
    for (; digs < width; ++digs)
    {
        putchar(' ');
    }
    printf("%lu", entry);
    return;
}

static int
table_print_break(const table_t *  table,
                  unsigned         cols,
                  const unsigned * digs,
                  unsigned         maxwidth)
{
    unsigned * fields = NULL;
    int        retval = 1;
    unsigned   width;

    fields = malloc(cols * sizeof(unsigned));
    if (!fields)
    {
        print_error_errno("could not allocate memory");
        goto error;
    }

    /* Compute the width of the total table for different number of
     * columns and stop as soon as the entire table fits. */
    do {
        memset(fields, 0, cols * sizeof(unsigned));
        width = 0;
        for (unsigned col = 0; col < cols && width <= maxwidth; ++col)
        {
            for (unsigned c = col; c < table->n_cols; c += cols)
            {
                if (digs[c] > fields[col])
                {
                    fields[col] = digs[c];
                }
            }
            if (col)
            {
                /* Increase the size of the field to include the space
                 * between this column and the previous. This is a nice
                 * way of not taking care of the space manually. */
                fields[col]++;
            }
            width += fields[col];
        }
    } while (width > maxwidth && --cols);
    /* The while condition must be read with care. First the width
     * is checked, to find out if the table fits with this number of
     * columns. If this is the case the while loop breaks without even
     * touching cols (which now contain the best number of columns).
     * One realises that if the checks were interchanged one would end
     * up with the final table always being one column too small.
     *
     * Anyway, if the table does not fit, then cols is decremented and
     * after that checked so it is not zero. This way cols=0 will never
     * be passed into the loop, and this case can be caught in the next
     * if-clause. */

    if (!cols)
    {
        print_error("could not fit table")
        goto error;
    }

    /* Finally print the table. */
    for (unsigned base = 0; base < table->n_cols; base += cols)
    {
        unsigned colmax = cols;
        if (base + colmax > table->n_cols)
        {
            colmax = table->n_cols - base;
        }

        for (unsigned row = 0; row < table->n_rows; ++row)
        {
            table_print_label(table, row);
            for (unsigned col = 0; col < colmax; ++col)
            {
                table_print_entry_rjust(table,
                                        row,
                                        base + col,
                                        fields[col]);
            }
            putchar('\n');
        }
    }

    retval = 0;
error:
    free(fields);
    return retval;
}

static void
table_print_nobreak(const table_t * table,
                    const unsigned * digs)
{
    for (unsigned row = 0; row < table->n_rows; ++row)
    {
        table_print_label(table, row);
        for (unsigned col = 0; col < table->n_cols; ++col)
        {
            if (col)
            {
                putchar(' ');
            }
            table_print_entry_rjust(table, row, col, digs[col]);
        }
        putchar('\n');
    }
    return;
}

int
table_print(const table_t * table)
{
    unsigned   cols;
    unsigned   dig;
    unsigned * digs;
    unsigned   index;
    unsigned   maxwidth = WIDTH;
    int        retval   = 1;
    unsigned   width;

    if (table->maxlabel)
    {
        /* +2 for colon and space */
        maxwidth -= table->maxlabel + 2;
    }

    /* Avoid recomputing number of digits each column occupies, by
     * saving this information in a table. */
    digs = calloc(table->n_cols, sizeof(unsigned));
    if (!digs)
    {
        print_error("could not allocate memory");
        return 1;
    }
    index = 0;
    for (unsigned c = 0; c < table->n_cols; ++c)
    {
        for (unsigned r = 0; r < table->n_rows; ++r)
        {
            dig = digits(table->entries[index]);
            if (dig > digs[c])
            {
                digs[c] = dig;
            }
            index++;
        }
    }

    /* Compute the maximum number of columns to include before the line
     * overflows. This is the maximum possible amount of columns in the
     * line broken output. */
    cols  = 0;
    width = 0;
    do {
        if (cols)
        {
            /* Space needed. */
            width++;
        }
        width += digs[cols];
    } while (width <= maxwidth && ++cols < table->n_cols);
    /* The conditions in this while loop may not be interchanged! The
     * interpretation of the condition is:
     * (1) If the width is already exceeded, break now. The cols
     *     now holds the number of columns that may fit, since cols
     *     was not incremented by the second condition to include the
     *     tested column (of index cols).
     * (2) If the width was not exceeded, it is know the column with
     *     index cols fitted, which means cols can be incremented, to
     *     include the fitting column. If all columns are now exhausted
     *     the loop is done. Otherwise do one more run. */

    if (width > maxwidth)
    {
        if (cols)
        {
            retval = table_print_break(table, cols, digs, maxwidth);
        }
        else
        {
            print_error("could not fit table");
        }
    }
    else /* if (cols == table->n_cols) */
    {
        /* Everything fits on one line no need to line break */
        table_print_nobreak(table, digs);
        retval = 0;
    }

    free(digs);
    return retval;
}
