/*
 * dist.c
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "dist.h"
#include "print-error.h"
#include "table.h"

void
dist_init(dist_t * dist)
{
    dist->size = 0;
    dist->array = NULL;
    return;
}

void
dist_deinit(dist_t * dist)
{
    free(dist->array);
    return;
}

int
dist_count(dist_t * dist, unsigned index)
{
    if (index >= dist->size)
    {
        unsigned new_size;
        unsigned long * new_array;

        new_size = index + 1;
        new_array = realloc(dist->array,
                            new_size * sizeof(unsigned long));
        if (new_array == NULL)
        {
            print_error_errno("could not allocate memory");
            return 1;
        }

        memset(new_array + dist->size,
               0,
               (new_size - dist->size) * sizeof(unsigned long));

        dist->size = new_size;
        dist->array = new_array;
    }

    dist->array[index]++;

    return 0;
}

unsigned long PURE
dist_get(const dist_t * dist, unsigned index)
{
    if (index < dist->size)
    {
        return dist->array[index];
    }
    else
    {
        return 0;
    }
}

int
dist_print(const dist_t * dist,
           int            print_total,
           const char *   indexlabel,
           const char *   countlabel)
{
    int           retval = 1;
    table_t *     table  = NULL;
    unsigned      cols   = 0;
    unsigned long total  = 0;
    unsigned      col;

    for (unsigned index = 0; index < dist->size; ++index)
    {
        unsigned long count = dist_get(dist, index);
        if (count)
        {
            cols++;
            total += count;
        }
    }

    if (!cols)
    {
        printf("(empty)\n");
        return 0;
    }

    table = table_new(2, cols, indexlabel, countlabel);
    if (!table)
    {
        goto error;
    }

    col = 0;
    for (unsigned index = 0; index < dist->size; ++index)
    {
        unsigned long count = dist_get(dist, index);
        if (count)
        {
            table_entry(table, 0, col, index);
            table_entry(table, 1, col, count);
            col++;
        }
    }

    if (table_print(table))
    {
        goto error;
    }
    if (print_total)
    {
        printf("total: %lu\n", total);
    }

    retval = 0;
error:
    table_destroy(table);
    return retval;
}
