/*
 * alg.c
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <time.h>

#include "alg.h"
#include "dist.h"
#include "print-error.h"
#include "zpset.h"
#include "zpset-list.h"

int
alg_begin(alg_data_t * data)
{
    if (data->time)
    {
        if (clock_gettime(CLOCK_THREAD_CPUTIME_ID, &data->t1))
        {
            print_error_errno("clock_gettime failed");
            return 1;
        }
    }
    return alg_update_max(data, 0);
}

int
alg_end(alg_data_t * data)
{
    if (data->time)
    {
        if (clock_gettime(CLOCK_THREAD_CPUTIME_ID, &data->t2))
        {
            print_error_errno("clock_gettime failed");
            return 1;
        }
    }
    return 0;
}

long
alg_time(const alg_data_t * data)
{
    if (data->time)
    {
        return (data->t2.tv_sec  - data->t1.tv_sec) * 1000000000 +
                data->t2.tv_nsec - data->t1.tv_nsec;
    }
    else
    {
        return 0;
    }
}

int
alg_update_max(alg_data_t * data,
               zpset_elm_t  count)
{
    data->maxsize = count;
    if (data->max)
    {
        zpset_list_clear(data->max);
    }
    if (data->complement)
    {
        if (zpset_fill(data->complement, data->n))
        {
            return 1;
        }
    }
    if (data->intersect)
    {
        if (zpset_fill(data->intersect, data->n))
        {
            return 1;
        }
    }
    return 0;
}

int
alg_register(alg_data_t * data,
             zpset_t *    cand,
             zpset_elm_t  count)
{
    if (!count)
    {
        count = zpset_count(cand);
    }

    if (data->all)
    {
        if (zpset_list_append(data->all, cand))
        {
            return 1;
        }
    }

    if (count > data->maxsize)
    {
        if (alg_update_max(data, count))
        {
            return 1;
        }
    }
    if (count == data->maxsize)
    {
        if (data->max)
        {
            if (zpset_list_append(data->max, cand))
            {
                return 1;
            }
        }
        if (data->complement)
        {
            zpset_minus_here(data->complement, cand);
        }
        if (data->intersect)
        {
            zpset_intersect_here(data->intersect, cand);
        }
    }

    if (data->dist)
    {
        if (dist_count(data->dist, count))
        {
            return 1;
        }
    }

    return 0;
}

int
alg_register_copy(alg_data_t * data,
                  const zpset_t * cand,
                  zpset_elm_t count)
{
    int       retval;
    zpset_t * copy;

    copy = zpset_dup(cand);
    if (!copy)
    {
        return 1;
    }

    retval = alg_register(data, copy, count);
    zpset_unref(copy);
    return retval;
}
