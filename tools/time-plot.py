#!/usr/bin/env python3
#
#  time-plot.py
#
#  Copyright 2015 Karl Lindén <karl.linden.887@student.lu.se>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

import argparse
import sys

import numpy as np
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='Plot computation times')
parser.add_argument('files', metavar='FILES', type=str, nargs='+',
                    help='files that contain the data to plot')
parser.add_argument('-o', '--output', type=str, default=None,
                    help='output file')
parser.add_argument('-l', '--log', action='store_const', const=True,
                    default=False)
parser.add_argument('-x', '--xlabel', type=str, help='xlabel')
parser.add_argument('-y', '--ylabel', type=str, help='ylabel')
parser.add_argument('-t', '--title', type=str, help='plot title')

args = parser.parse_args()

if not args.output:
    print('No output file given')
    sys.exit(1)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

if args.title:
    plt.title(args.title)
else:
    plt.title(r'Computation time versus $n$')
if args.xlabel:
    plt.xlabel(args.xlabel)
else:
    plt.xlabel(r'$n$')
if args.ylabel:
    plt.ylabel(args.ylabel)
else:
    plt.ylabel(r'Computation time (nsec)')
if args.log:
    plt.yscale('log')

for fname in args.files:
    if ':' in fname:
        fname, label = fname.split(':')
    else:
        label = fname
    with open(fname) as f:
        ns = []
        mins = []
        means = []
        maxes = []
        for line in f:
            fields = line.split(' ')
            ns.append(int(fields.pop(0).strip(':')))
            arr = [int(t) for t in fields]
            mins.append(np.min(arr))
            means.append(np.mean(arr))
            maxes.append(np.max(arr))
        plt.errorbar(ns, means, yerr=[mins, maxes], label=label)

plt.grid(True, which='both')
plt.legend(loc='upper left')

plt.savefig(args.output)
