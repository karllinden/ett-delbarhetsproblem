/*
 * rng.h
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#ifndef RNG_H
# define RNG_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <gop.h>

void     rng_seed_exact(unsigned short seed[3]);
void     rng_seed(void);
long int rng_max(long int max);
long int rng_min_max(long int min, long int max);
void     rng_seed_to_hex(const unsigned short seed[3], char hex[13]);
int      rng_hex_to_seed(const char hex[13], unsigned short seed[3]);

void     rng_add_gop_table(gop_t * const gop);

#endif /* !RNG_H */
