/*
 * zpset-dta-max.c
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <stdlib.h>

#include <gop.h>

#include "print-error.h"
#include "rng.h"
#include "rng-dta.h"
#include "zpset.h"

#define RANDOM_TESTS 10000

static int
test_expect(zpset_dta_t bitmask, unsigned int bit)
{
    if (bit != zpset_dta_max(bitmask))
    {
        print_error("zpset_dta_max(0x%lx) = %u != %u",
                    bitmask, zpset_dta_max(bitmask), bit);
        return 1;
    }
    return 0;
}

static int
test_one_bit(void)
{
    for (unsigned int bit = 0; bit < 8 * sizeof(zpset_dta_t); ++bit)
    {
        if (test_expect(1lu << bit, bit))
        {
            return 1;
        }
    }
    return 0;
}

static int
test(zpset_dta_t bitmask)
{
    assert(bitmask);

    /* Compute the maximum bit in a stupid way. */
    unsigned int bit = 8 * sizeof(zpset_dta_t) - 1;
    while (!(bitmask & (1lu << bit)))
    {
        bit--;
    }
    return test_expect(bitmask, bit);
}

static int
test_randomly(unsigned int tests)
{
    zpset_dta_t rnd;
    while (tests--)
    {
        /* The random number generator cannot generate 64-bits of pseudo
         * random data, so generate 2*31. */
        do {
            rnd = rng_dta();
        } while (!rnd);
        if (test(rnd))
        {
            return 1;
        }
    }
    return 0;
}

int
main(int argc, char ** const argv)
{
    int exit_status = EXIT_FAILURE;

    gop_t * const gop = gop_new();
    if (gop == NULL)
    {
        goto error;
    }

    rng_add_gop_table(gop);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    rng_seed();

    if (test_one_bit())
    {
        goto error;
    }

    if (test_randomly(RANDOM_TESTS))
    {
        goto error;
    }

    exit_status = EXIT_SUCCESS;
error:
    return exit_status;
}
