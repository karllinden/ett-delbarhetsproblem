#!/bin/bash
#
#  alg-check.sh
#
#  Copyright 2015 Karl Lindén <karl.j.linden@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

# Check if algorithm implementations work by comparing outputs of the
# print program.

# USAGE: alg-check.sh NMAX BUILDDIR alg1 alg2 [...]

die() {
    echo "error: ${@}"
    exit 1
}

: ${ARGS:=-msci}

if [ $# -lt 4 ]
then
    die "invalid number or arguments"
fi

nmax=${1}
shift
builddir="${1}"
shift
algs=(${@})
files=()

for alg in ${algs[@]}
do
    file=$(mktemp out-${alg}.txt.XXXXXXXX)
    files+=("${file}")
    "${builddir}"/print-${alg} $(seq 1 ${nmax}) ${ARGS} > ${file} \
        || die "print-${alg} failed"
done

for file in ${files[@]:1}
do
    diff -u ${files[0]} ${file}
    if [ $? -ne 0 ]
    then
        die "${files[0]} and ${file} differed"
    fi
done

for file in ${files[@]}
do
    rm "${file}"
done

exit 0
