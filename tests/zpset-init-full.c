/*
 * zpset-init-full.c
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>

#include <gop.h>

#include "print-error.h"
#include "rng.h"
#include "zpset.h"

/* FIXME: respect future ZPSET_ELM_MAX */
#define ELM_MAX    10000
#define RUNS         100
#define TEST_EXTRA   100

int
main(int argc, char ** const argv)
{
    int exit_status = EXIT_FAILURE;

    gop_t * const gop = gop_new();
    if (gop == NULL)
    {
        goto error;
    }

    rng_add_gop_table(gop);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    rng_seed();

    for (unsigned int i = 0; i < RUNS; ++i)
    {
        zpset_elm_t elms = (zpset_elm_t)rng_min_max(1, ELM_MAX);
        zpset_t zpset;
        if (zpset_init_full(&zpset, elms))
        {
            goto error;
        }
        for (zpset_elm_t elm = 1; elm <= elms; ++elm)
        {
            if (!zpset_contains(&zpset, elm))
            {
                print_error("%u is not contained in full set with %u "
                            "elements", elm, elms);
                goto error;
            }
        }
        for (zpset_elm_t extra = 1; extra < TEST_EXTRA; ++extra)
        {
            if (zpset_contains(&zpset, elms + extra))
            {
                print_error("%u is contained in full set with %u "
                            "elements", elms + extra, elms);
                goto error;
            }
        }
        zpset_deinit(&zpset);
    }

    exit_status = EXIT_SUCCESS;
error:
    return exit_status;
}
