/*
 * zpset-remove-less.c
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>

#include "print-error.h"
#include "zpset.h"

/* FIXME: respect future ZPSET_ELM_MAX */
#define ELM_MAX 512

int
main(int argc, char ** const argv)
{
    int exit_status = EXIT_FAILURE;

    zpset_t zpset;
    zpset_init(&zpset);

    for (zpset_elm_t max = 1; max < ELM_MAX; ++max)
    {
        for (zpset_elm_t r = 1; r <= max; ++r)
        {
            if (zpset_fill(&zpset, max))
            {
                goto error;
            }
            zpset_remove_less(&zpset, r);
            for (zpset_elm_t e = 1; e < r; ++e)
            {
                if (zpset_contains(&zpset, e))
                {
                    print_error("%u is contained in set from which it "
                                "should have been removed", e);
                    goto error;
                }
            }
            for (zpset_elm_t e = r; e <= max; ++e)
            {
                if (!zpset_contains(&zpset, e))
                {
                    print_error("%u is not contained in a set that "
                                "should contain it", e);
                    goto error;
                }
            }
        }
    }

    exit_status = EXIT_SUCCESS;
error:
    zpset_deinit(&zpset);
    return exit_status;
}
