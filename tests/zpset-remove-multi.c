/*
 * zpset-remove-multi.c
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>

#include "print-error.h"
#include "zpset.h"

/* FIXME: respect future ZPSET_ELM_MAX */
#define ELM_MAX 1024

int
main(int argc, char ** const argv)
{
    int exit_status = EXIT_FAILURE;

    zpset_t zpset;
    zpset_init(&zpset);

    for (zpset_elm_t elm = 1; elm <= ELM_MAX; ++elm)
    {
        if (zpset_fill(&zpset, ELM_MAX))
        {
            goto error;
        }
        zpset_remove_multi(&zpset, elm);
        for (zpset_elm_t e = 1; e <= ELM_MAX; ++e)
        {
            if (e % elm)
            {
                if (!zpset_contains(&zpset, e))
                {
                    print_error("%u is not contained in set from which "
                                "only multiples of %u should have been "
                                "removed", e, elm);
                    goto error;
                }
            }
            else
            {
                if (zpset_contains(&zpset, e))
                {
                    print_error("%u is contained in set from which "
                                "multiples of %u should have been "
                                "removed", e, elm);
                    goto error;
                }
            }
        }
    }

    exit_status = EXIT_SUCCESS;
error:
    zpset_deinit(&zpset);
    return exit_status;
}
