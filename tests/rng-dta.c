/*
 * rng-dta.c
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <limits.h>

#include "rng.h"
#include "rng-dta.h"
#include "zpset.h"

zpset_dta_t
rng_dta(void)
{
    zpset_dta_t rnd = 0;
    rnd = (zpset_dta_t)rng_min_max(0, INT_MAX);
#if SIZEOF_UNSIGNED_LONG == 8
    rnd <<= 32;
    rnd |= (zpset_dta_t)rng_min_max(0, INT_MAX);
#endif
    return rnd;
}
