/*
 * zpset-test.c
 *
 * Copyright (C) 2015 Karl Linden <karl.linden.887@student.lu.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>

#include <gop.h>

#include "attributes.h"
#include "print-error.h"
#include "rng.h"
#include "zpset.h"

/* FIXME: respect future ZPSET_ELM_MAX */
#define ELM_MAX 4096
#define SETS      16

struct bt_node_s
{
    zpset_elm_t        key;
    struct bt_node_s * left;
    struct bt_node_s * right;
};
typedef struct bt_node_s bt_node_t;

struct bt_s
{
    bt_node_t *  root;
    unsigned int size;
};
typedef struct bt_s bt_t;

struct expect_recurse_s
{
    const zpset_t * set;
    const bt_t * other;
};
typedef struct expect_recurse_s expect_recurse_t;

static bt_t    trees[SETS];
static zpset_t sets[SETS];

static void *
xmalloc(size_t size)
{
    if (size == 0)
    {
        size = 1;
    }
    void * ptr = malloc(size);
    if (ptr == NULL)
    {
        print_error("could not allocate memory");
        abort();
    }
    return ptr;
}

static bt_node_t *
bt_node_new(zpset_elm_t key)
{
    bt_node_t * node = xmalloc(sizeof(bt_node_t));
    node->key = key;
    node->left = NULL;
    node->right = NULL;
    return node;
}

static void
bt_node_destroy(bt_node_t * node)
{
    if (node != NULL)
    {
        bt_node_destroy(node->left);
        bt_node_destroy(node->right);
    }
    free(node);
    return;
}

static int
bt_node_recurse(const bt_node_t * node,
                zpset_func_t * func,
                void * data)
{
    if (node != NULL)
    {
        int ret;

        ret = bt_node_recurse(node->left, func, data);
        if (ret)
        {
            return ret;
        }

        ret = func(node->key, data);
        if (ret)
        {
            return ret;
        }

        ret = bt_node_recurse(node->right, func, data);
        if (ret)
        {
            return ret;
        }
    }
    return 0;
}

static void
bt_init(bt_t * bt)
{
    bt->root = NULL;
    bt->size = 0;
    return;
}

static void
bt_deinit(bt_t * bt)
{
    bt_node_destroy(bt->root);
    bt->root = NULL;
    return;
}

static int PURE
bt_contains(const bt_t * bt, zpset_elm_t key)
{
    const bt_node_t * node = bt->root;
    while (node != NULL)
    {
        if (key < node->key)
        {
            node = node->left;
        }
        else if (key > node->key)
        {
            node = node->right;
        }
        else
        {
            return 1;
        }
    }
    return 0;
}

/* Returns 0 if insertion succeeded or 1 if the key already exists in
 * the binary tree. */
static int
bt_insert(bt_t * bt,
          zpset_elm_t key)
{
    bt_node_t ** parentp = &bt->root;
    bt_node_t * parent;
    while (*parentp != NULL)
    {
        parent = *parentp;
        if (key < parent->key)
        {
            parentp = &parent->left;
        }
        else if (key > parent->key)
        {
            parentp = &parent->right;
        }
        else
        {
            return 1;
        }
    }
    *parentp = bt_node_new(key);
    bt->size++;
    return 0;
}

/* Deletes an element from the binary tree. Returns 0 if the element was
 * successfully deleted or 1 if the element was not found. */
static int
bt_delete(bt_t * bt, zpset_elm_t key)
{
    bt_node_t ** nodep = &bt->root;
    bt_node_t * node;
    while (*nodep != NULL)
    {
        node = *nodep;
        if (key < node->key)
        {
            nodep = &node->left;
        }
        else if (key > node->key)
        {
            nodep = &node->right;
        }
        else
        {
            /* Unlink this node from the tree. */
            if (node->left == NULL)
            {
                *nodep = node->right;
                node->right = NULL;
            }
            else if (node->right == NULL)
            {
                *nodep = node->left;
                node->left = NULL;
            }
            else
            {
                /* The two branches have to be merged. */
                bt_node_t * left = node->left;
                bt_node_t * right = node->right;
                if (left->right == NULL)
                {
                    *nodep = left;
                    left->right = right;
                }
                else
                {
                    *nodep = right;
                    while (right->left != NULL)
                    {
                        right = right->left;
                    }
                    right->left = left;
                }
                node->left = NULL;
                node->right = NULL;
            }

            /* Finally! Destroy the node. */
            bt_node_destroy(node);
            return 0;
        }
    }
    return 1;
}

static int
bt_recurse(const bt_t * bt,
           zpset_func_t * func,
           void * data)
{
    return bt_node_recurse(bt->root, func, data);
}

static int
bt_count_recurse(zpset_elm_t e, void * data)
{
    unsigned int * countp = data;
    (*countp)++;
    return 0;
}

static unsigned int
bt_count(const bt_t * bt)
{
    unsigned int count = 0;
    bt_recurse(bt, &bt_count_recurse, &count);
    return count;
}

static zpset_elm_t PURE
bt_min(const bt_t * bt)
{
    if (bt->root != NULL)
    {
        bt_node_t * node = bt->root;
        while (node->left != NULL)
        {
            node = node->left;
        }
        return node->key;
    }
    return 0;
}

static zpset_elm_t PURE
bt_max(const bt_t * bt)
{
    if (bt->root != NULL)
    {
        bt_node_t * node = bt->root;
        while (node->right != NULL)
        {
            node = node->right;
        }
        return node->key;
    }
    return 0;
}

static int
bt_array_recurse(zpset_elm_t e, void * data)
{
    zpset_elm_t ** arrayp = data;
    **arrayp = e;
    (*arrayp)++;
    return 0;
}

/* Create a dynamically allocated array of the elements in the binary
 * tree. */
static zpset_elm_t *
bt_array(const bt_t * bt, unsigned int * countp)
{
    unsigned int count;
    zpset_elm_t * array;
    zpset_elm_t * arrayc;

    count = bt_count(bt);
    array = xmalloc(count * sizeof(zpset_elm_t));

    /* Copy the pointer to the array into a pointer which is writable
     * to the recursion functions. */
    arrayc = array;
    bt_recurse(bt, &bt_array_recurse, &arrayc);

    *countp = count;
    return array;
}

static int
create(void)
{
    for (unsigned int i = 0; i < SETS; ++i)
    {
        bt_t *      bt = trees + i;
        zpset_t *   set = sets + i;
        zpset_elm_t elms;

        bt_init(bt);
        zpset_init(set);

        elms = (zpset_elm_t)rng_max(ELM_MAX / 2);
        for (unsigned int j = 0; j < elms; ++j)
        {
            zpset_elm_t e;
            do {
                e = (zpset_elm_t)rng_min_max(1, ELM_MAX);
            }
            while (bt_insert(bt, e));
            if (zpset_add(set, e))
            {
                return 1;
            }
        }

        /* Make sure the zpset_is_empty function works. */
        if (elms)
        {
            if (zpset_is_empty(set))
            {
                print_error("zpset_is_empty returned non-zero for non-empty "
                            "set");
                return 1;
            }
        }
        else
        {
            if (!zpset_is_empty(set))
            {
                print_error("zpset_is_empty returned zero for empty set");
                return 1;
            }
        }
    }
    return 0;
}

static void
destroy(void)
{
    for (unsigned int i = 0; i < SETS; ++i)
    {
        bt_deinit(trees + i);
        zpset_deinit(sets + i);
    }
    return;
}

static int
count(void)
{
    for (unsigned int i = 0; i < SETS; ++i)
    {
        bt_t *    bt = trees + i;
        zpset_t * set = sets + i;

        if (bt_count(bt) != zpset_count(set))
        {
            print_error("count mismatch, %u (bt) versus %u (set)",
                        bt_count(bt), zpset_count(set));
            return 1;
        }
    }
    return 0;
}

static int
min(void)
{
    for (unsigned int i = 0; i < SETS; ++i)
    {
        bt_t *    bt = trees + i;
        zpset_t * set = sets + i;

        if (bt_min(bt) != zpset_min(set))
        {
            print_error("min mismatch, %u (bt) versus %u (set)",
                        bt_min(bt), zpset_min(set));
            return 1;
        }
    }
    return 0;
}

static int
max(void)
{
    for (unsigned int i = 0; i < SETS; ++i)
    {
        bt_t *    bt = trees + i;
        zpset_t * set = sets + i;

        if (bt_max(bt) != zpset_max(set))
        {
            print_error("max mismatch, %u (bt) versus %u (set)",
                        bt_max(bt), zpset_max(set));
            return 1;
        }
    }
    return 0;
}

static int
search_recurse(zpset_elm_t e, void * data)
{
    zpset_t * set = data;
    if (!zpset_contains(set, e))
    {
        print_error("set does not contain %u", e);
        return 1;
    }
    return 0;
}

static int
search_positive(void)
{
    for (unsigned int i = 0; i < SETS; ++i)
    {
        bt_t *    bt  = trees + i;
        zpset_t * set = sets + i;

        if (bt_recurse(bt, &search_recurse, set))
        {
            return 1;
        }
    }
    return 0;
}

static int
search_negative(void)
{
    for (unsigned int i = 0; i < SETS; ++i)
    {
        bt_t *       bt  = trees + i;
        zpset_t *    set = sets + i;
        unsigned int n;

        /* The /4 is rather arbitrary, but it prevents it from searching
         * for too long. */
        n = (unsigned int)rng_max(ELM_MAX / 4);
        for (unsigned int j = 0; j < n; ++j)
        {
            zpset_elm_t e;
            do {
                e = (zpset_elm_t)rng_min_max(1, ELM_MAX);
            } while (bt_contains(bt, e));
            if (zpset_contains(set, e))
            {
                print_error("set contained %u\n", e);
                return 1;
            }
        }
    }
    return 0;
}

static int
unions_here(void)
{
    int retval = 1;
    zpset_t u;
    zpset_init(&u);

    for (unsigned int i = 0; i < SETS; ++i)
    {
        bt_t *    atree = trees + i;
        zpset_t * aset  = sets + i;
        for (unsigned int j = i + 1; j < SETS; ++j)
        {
            bt_t *    btree = trees + j;
            zpset_t * bset  = sets + j;
            if (zpset_cpy(&u, aset))
            {
                goto error;
            }
            if (zpset_union_here(&u, bset))
            {
                goto error;
            }
            if (bt_recurse(atree, &search_recurse, &u) ||
                bt_recurse(btree, &search_recurse, &u))
            {
                goto error;
            }
        }
    }

    retval = 0;
error:
    zpset_deinit(&u);
    return retval;
}

static int
unions(void)
{
    for (unsigned int i = 0; i < SETS; ++i)
    {
        bt_t *    atree = trees + i;
        zpset_t * aset  = sets + i;
        for (unsigned int j = i + 1; j < SETS; ++j)
        {
            bt_t *    btree = trees + j;
            zpset_t * bset  = sets + j;
            zpset_t * u;

            u = zpset_union(aset, bset);
            if (u == NULL)
            {
                return 1;
            }
            if (bt_recurse(atree, &search_recurse, u) ||
                bt_recurse(btree, &search_recurse, u))
            {
                zpset_unref(u);
                return 1;
            }
            zpset_unref(u);
        }
    }
    return 0;
}

static int
intersection_recurse(zpset_elm_t e, void * data)
{
    expect_recurse_t * er = data;
    if (bt_contains(er->other, e))
    {
        if (!zpset_contains(er->set, e))
        {
            print_error("intersection did not contain %u", e);
            return 1;
        }
    }
    else
    {
        if (zpset_contains(er->set, e))
        {
            print_error("intersection contains %u", e);
            return 1;
        }
    }
    return 0;
}

static int
intersection_expect(const zpset_t * set,
                    const bt_t * atree,
                    const bt_t * btree)
{
    expect_recurse_t er;
    er.set = set;
    er.other = btree;
    return bt_recurse(atree, &intersection_recurse, &er);
}

static int
intersections_here(void)
{
    int retval = 1;
    zpset_t set;
    zpset_init(&set);

    for (unsigned int i = 0; i < SETS; ++i)
    {
        bt_t *    atree = trees + i;
        zpset_t * aset  = sets + i;
        for (unsigned int j = i + 1; j < SETS; ++j)
        {
            bt_t *    btree = trees + j;
            zpset_t * bset  = sets + j;
            if (zpset_cpy(&set, aset))
            {
                goto error;
            }
            zpset_intersect_here(&set, bset);
            if (intersection_expect(&set, atree, btree))
            {
                goto error;
            }
        }
    }

    retval = 0;
error:
    zpset_deinit(&set);
    return retval;
}

static int
intersections(void)
{
    for (unsigned int i = 0; i < SETS; ++i)
    {
        bt_t *    atree = trees + i;
        zpset_t * aset  = sets + i;
        for (unsigned int j = i + 1; j < SETS; ++j)
        {
            bt_t *    btree = trees + j;
            zpset_t * bset  = sets + j;
            zpset_t * set;
            set = zpset_intersect(aset, bset);
            if (set == NULL)
            {
                return 1;
            }
            if (intersection_expect(set, atree, btree))
            {
                zpset_unref(set);
                return 1;
            }
            zpset_unref(set);
        }
    }
    return 0;
}

static int
minus_recurse(zpset_elm_t e, void * data)
{
    expect_recurse_t * er = data;
    if (bt_contains(er->other, e))
    {
        if (zpset_contains(er->set, e))
        {
            print_error("minus contained %u", e);
            return 1;
        }
    }
    else
    {
        if (!zpset_contains(er->set, e))
        {
            print_error("minus did not contain %u", e);
            return 1;
        }
    }
    return 0;
}

static int
minus_expect(const zpset_t * set,
             const bt_t * atree,
             const bt_t * btree)
{
    expect_recurse_t er;
    er.set = set;
    er.other = btree;
    return bt_recurse(atree, &minus_recurse, &er);
}

static int
minus_here(void)
{
    int retval = 1;
    zpset_t set;
    zpset_init(&set);

    for (unsigned int i = 0; i < SETS; ++i)
    {
        bt_t *    atree = trees + i;
        zpset_t * aset  = sets + i;
        for (unsigned int j = 0; j < SETS; ++j)
        {
            bt_t *    btree = trees + j;
            zpset_t * bset  = sets + j;
            if (i != j)
            {
                if (zpset_cpy(&set, aset))
                {
                    goto error;
                }
                zpset_minus_here(&set, bset);
                if (minus_expect(&set, atree, btree))
                {
                    goto error;
                }
            }
        }
    }

    retval = 0;
error:
    zpset_deinit(&set);
    return retval;
}

static int
minus(void)
{
    for (unsigned int i = 0; i < SETS; ++i)
    {
        bt_t *    atree = trees + i;
        zpset_t * aset  = sets + i;
        for (unsigned int j = 0; j < SETS; ++j)
        {
            bt_t *    btree = trees + j;
            zpset_t * bset  = sets + j;
            if (i != j)
            {
                zpset_t * set;
                set = zpset_minus(aset, bset);
                if (set == NULL)
                {
                    return 1;
                }
                if (minus_expect(set, atree, btree))
                {
                    zpset_unref(set);
                    return 1;
                }
                zpset_unref(set);
            }
        }
    }
    return 0;
}

static int
delete(void)
{
    for (unsigned int i = 0; i < SETS; ++i)
    {
        bt_t * bt = trees + i;
        zpset_t * set = sets + i;
        unsigned int dcount; /* number of elements to delete */
        unsigned int count; /* number of elements in tree */
        zpset_elm_t * array = bt_array(bt, &count);

        dcount = (unsigned int)rng_max(count);
        for (unsigned int j = 0; j < dcount; ++j)
        {
            unsigned int index;
            zpset_elm_t e;
            int ret;

            count--;
            index = (unsigned int)rng_max(count);
            e = array[index];
            array[index] = array[count];

            ret = bt_delete(bt, e);
            if (ret)
            {
                /* Deletion was unsuccessful. */
                print_error("deletion of %u from bt failed", e);
                free(array);
                return 1;
            }

            zpset_remove(set, e);
        }

        free(array);
    }
    return 0;
}

static int
iterate(void)
{
    int             retval = 1;
    zpset_elm_t *   array  = NULL;

    for (unsigned int i = 0; i < SETS; ++i)
    {
        const bt_t *    bt = trees + i;
        const zpset_t * set = sets + i;
        unsigned        count;
        unsigned        index;
        zpset_iter_t    iter;
        zpset_elm_t     elm;

        array = bt_array(bt, &count);
        index = 0;

        zpset_iter_init(&iter, set);
        while ((elm = zpset_iter(&iter)))
        {
            if (index >= count)
            {
                print_error("too many elements in iteration");
                goto error;
            }
            else if (elm != array[index])
            {
                print_error("unexpected element %u during iteration, "
                            "expected %u", elm, array[index]);
                goto error;
            }
            index++;
        }

        free(array);
        array = NULL;
    }

    retval = 0;
error:
    free(array);
    return retval;
}

static int
all(void)
{
    int ret = 0;
    ret += count();
    ret += min();
    ret += max();
    ret += search_positive();
    ret += search_negative();
    ret += unions_here();
    ret += unions();
    ret += intersections_here();
    ret += intersections();
    ret += minus_here();
    ret += minus();
    ret += iterate();
    return ret;
}

int
main(int argc, char ** const argv)
{
    int exit_status = EXIT_FAILURE;

    gop_t * const gop = gop_new();
    if (gop == NULL)
    {
        goto error;
    }

    rng_add_gop_table(gop);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    rng_seed();

    if (create())
    {
        goto fail;
    }
    if (all())
    {
        goto fail;
    }
    if (delete())
    {
        goto fail;
    }
    if (all())
    {
        goto fail;
    }

    exit_status = EXIT_SUCCESS;
fail:
    destroy();
error:
    return exit_status;
}
